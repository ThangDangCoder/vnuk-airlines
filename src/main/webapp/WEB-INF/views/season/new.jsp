<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>


<c:import url="/resources/jsp/header_booking.jsp" />
    <h3>Create season</h3>
	<form action="../season/create" method="post">
        Season :<input type="text" name="name" />
        <br />
        
        <button type="submit" class="btn btn-success">
            <i class="fa fa-check" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Create
        </button>
        
        <a class="btn btn-default" href="../seasons">
            <i class="fa fa-times-rectangle-o" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Cancel
        </a>
    </form>
<c:import url="/resources/jsp/body-close.jsp" />