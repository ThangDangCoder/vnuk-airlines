<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>


<c:import url="/resources/jsp/header_booking.jsp" />
    <h3>Create Flight</h3>
	<form action="../flight/create" method="post">
		Route Id: <br/>
		<form:select path = "flight.routeId">
        	<form:options items = "${routeList}" />
        </form:select>
        <br/>
        Plane: <br/>
        <form:select path = "flight.planeId">
        	<form:options items = "${planeModelList}" />
        </form:select>
       	<br/>
       	Flight Code: <input type="text" name="flightCode" value="${flight.flightCode}"/>
       	<br/>
       	Day Id: <br/>
       	<form:select path = "flight.dayId">
        	<form:options items = "${dayList}" />
        </form:select>
       	<br/>
       	Departure Time: <input type="text" name="departureTime" value="${flight.departureTime}"/>
       	<br/>
       	Arrival Time: <input type="text" name="arrivalTime" value="${flight.arrivalTime}"/>
       	<br/>
        <button type="submit" class="btn btn-success">
            <i class="fa fa-check" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Create
        </button>
        
        <a class="btn btn-default" href="../flights">
            <i class="fa fa-times-rectangle-o" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Cancel
        </a>
    </form>
<c:import url="/resources/jsp/body-close.jsp" />