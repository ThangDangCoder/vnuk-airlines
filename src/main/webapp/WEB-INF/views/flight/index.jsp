<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:import url="/resources/jsp/header_booking.jsp" />

    <a href="flight/new" class="btn btn-primary">
        <i class="fa fa-plus" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;New Flight
    </a>

    <br /><br />
    
    <div id="my-notice"></div>
    
    <table class="table table-bordered table-hover table-responsive table-striped">
        <thead>
            <tr>
                <th></th>
                <th>Id</th>
                <th>Route</th>
                <th>Plane</th>
                <th>Flight Code</th>
                <th>Day</th>
                <th>Departure Time</th>
                <th>Arrival Time</th>
            </tr>
        </thead>

        <tbody>

            <c:forEach var="flight" items="${flights}">

                <tr>
                    <td>
                    	<a href="flight/show?id=${flight.id}" class="btn btn-xs btn-success" 
                           data-toggle="tooltip" title="Show flight" data-placement="bottom">
                            <i class="fa fa-eye" aria-hidden="true"></i>
                        </a>
                        
                        <a href="flight/edit?id=${flight.id}" class="btn btn-xs btn-primary" 
                           data-toggle="tooltip" title="Edit flight" data-placement="bottom">
                            <i class="fa fa-pencil" aria-hidden="true"></i>
                        </a>
                       
                       <%--    DELETING WITH AJAX   --%>

                        <button type="button"
                                class="btn btn-xs btn-danger my-flight-to-delete" 
                                value="${flight.id}"
                                data-toggle="tooltip" 
                                title="Delete flight" 
                                data-placement="bottom">
                            <i class="fa fa-trash" aria-hidden="true"></i>
                        </button>

                    </td>
                    
                    <td>${flight.id}</td>
                    <td>${flight.route.airportFrom.code} - ${flight.route.airportTo.code} </td>
                    <td>${flight.plane.planeModel.name}</td>
                    <td>${flight.flightCode}</td>
                    <td>${flight.day.name}</td>
                    <td>${flight.departureDate} - ${flight.departureTime}</td>
                    <td>${flight.arrivalDate} - ${flight.arrivalTime}</td>
                    
               </tr>

            </c:forEach>

        </tbody>
    </table>
            
<c:import url="/resources/jsp/body-close-flights.jsp" />
