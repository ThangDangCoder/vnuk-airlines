<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:import url="/resources/jsp/header_booking.jsp" />

    <a href="country/new" class="btn btn-primary">
        <i class="fa fa-plus" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;New Country
    </a>

    <br/><br />
    
    <div id="my-notice"></div>
    
    <table class="table table-bordered table-hover table-responsive table-striped">
        <thead>
            <tr>
                <th></th>
                <th>Id</th>
                <th>Name</th>
            </tr>
        </thead>

        <tbody>

            <c:forEach var="country" items="${countries}">

                <tr>
                    <td>
                    	<a href="country/show?id=${country.id}" class="btn btn-xs btn-success" 
                           data-toggle="tooltip" title="Show country" data-placement="bottom">
                            <i class="fa fa-eye" aria-hidden="true"></i>
                        </a>
                    	
                        <a href="country/edit?id=${country.id}" class="btn btn-xs btn-primary" 
                           data-toggle="tooltip" title="Edit country" data-placement="bottom">
                            <i class="fa fa-pencil" aria-hidden="true"></i>
                        </a>
                       
                       <%--    DELETING WITH AJAX   --%>

                        <button type="button"
                                class="btn btn-xs btn-danger my-country-to-delete" 
                                value="${country.id}"
                                data-toggle="tooltip" 
                                title="Delete country" 
                                data-placement="bottom">
                            <i class="fa fa-trash" aria-hidden="true"></i>
                        </button>

                    </td>
                    
                    <td>${country.id}</td>
                    <td>${country.name}</td>
                    
               </tr>

            </c:forEach>

        </tbody>
    </table>
            
<c:import url="/resources/jsp/body-close-countries.jsp" />
