<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>


<c:import url="/resources/jsp/header_booking.jsp" />
    <h3>Show detail country ${country.id}</h3>

    <form action="../country/update" method="post">
	    <table class="table table-bordered table-hover table-responsive table-striped">
	        <thead>
	            <tr>
	                <th>Id</th>
	                <th>Name</th>
	            </tr>
	        </thead>
			
	        <tbody>
	        	<tr>
		        	<td>${country.id}</td>
		        	<td>${country.name}</td>
	        	</tr>
	        </tbody>
	    </table>

        <a class="btn btn-default" href="../countries">
            <i class="fa fa-times-rectangle-o" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Cancel
        </a>
    </form>
<c:import url="/resources/jsp/body-close.jsp" />
