<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:import url="/resources/jsp/header_booking.jsp" />

    <a href="planeManufacture/new" class="btn btn-primary">
        <i class="fa fa-plus" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;New Plane Manufacture
    </a>

    <br /><br />
    
    <div id="my-notice"></div>
    
    <table class="table table-bordered table-hover table-responsive table-striped">
        <thead>
            <tr>
                <th></th>
                <th>Id</th>
                <th>Name</th>
            </tr>
        </thead>

        <tbody>

            <c:forEach var="planeManufacture" items="${planeManufactures}">

                <tr>
                    <td>
                   	 	<a href="planeManufacture/show?id=${planeManufacture.id}" class="btn btn-xs btn-success" 
                           data-toggle="tooltip" title="Show plane Manufacture" data-placement="bottom">
                            <i class="fa fa-eye" aria-hidden="true"></i>
                        </a>
                        <a href="planeManufacture/edit?id=${planeManufacture.id}" class="btn btn-xs btn-primary" 
                           data-toggle="tooltip" title="Edit planeManufacture" data-placement="bottom">
                            <i class="fa fa-pencil" aria-hidden="true"></i>
                        </a>
                       
                       <%--    DELETING WITH AJAX   --%>

                        <button type="button"
                                class="btn btn-xs btn-danger my-planeManufacture-to-delete" 
                                value="${planeManufacture.id}"
                                data-toggle="tooltip" 
                                title="Delete planeManufacture" 
                                data-placement="bottom">
                            <i class="fa fa-trash" aria-hidden="true"></i>
                        </button>

                    </td>
                    
                    <td>${planeManufacture.id}</td>
                    <td>${planeManufacture.name}</td>
                    
               </tr>

            </c:forEach>

        </tbody>
    </table>
            
<c:import url="/resources/jsp/body-close-planeManufactures.jsp" />
