<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>


<c:import url="/resources/jsp/body-open.jsp" />
    <h3>Create airport</h3>
	<form action="../airport/create" method="post">
        Code : <br />
        <input type="text" name="code" />
        <br />
        City Id : <br />
         <form:select path = "airport.cityId">
         	 <form:options items = "${cityList}" />
          </form:select> 
          <br/>	
        <button type="submit" class="btn btn-success">
            <i class="fa fa-check" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Create
        </button>
        <a class="btn btn-default" href="../airports">
            <i class="fa fa-times-rectangle-o" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Cancel
        </a>
    </form>
<c:import url="/resources/jsp/body-close.jsp" />