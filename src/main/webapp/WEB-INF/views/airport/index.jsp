<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:import url="/resources/jsp/header_booking.jsp" />
    <a href="airport/new" class="btn btn-primary">
        <i class="fa fa-plus" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;New Airport
    </a>

    <br /><br />
    
    <div id="my-notice"></div>
    
    <table class="table table-bordered table-hover table-responsive table-striped">
        <thead>
            <tr>
                <th></th>
                <th>Id</th>
                <th>Code</th>
                <th>City</th>
            </tr>
        </thead>

        <tbody>

            <c:forEach var="airport" items="${airports}">

                <tr>
                    <td>
                    	
                        <a href="airport/show?id=${airport.id}" class="btn btn-xs btn-success" 
                           data-toggle="tooltip" title="Show airport" data-placement="bottom">
                            <i class="fa fa-eye" aria-hidden="true"></i>
                        </a>
                        
                        <a href="airport/edit?id=${airport.id}" class="btn btn-xs btn-primary" 
                           data-toggle="tooltip" title="Edit airport" data-placement="bottom">
                            <i class="fa fa-pencil" aria-hidden="true"></i>
                        </a>
                       
                       <%--    DELETING WITH AJAX   --%>

                        <button type="button"
                                class="btn btn-xs btn-danger my-airport-to-delete" 
                                value="${airport.id}"
                                data-toggle="tooltip" 
                                title="Delete airport" 
                                data-placement="bottom">
                            <i class="fa fa-trash" aria-hidden="true"></i>
                        </button>

                    </td>
                    
                    <td>${airport.id}</td>
                    <td>${airport.code}</td>
                    <td>${airport.city.name}</td>
                    
               </tr>

            </c:forEach>

        </tbody>
    </table>
            
<c:import url="/resources/jsp/body-close-airports.jsp" />
