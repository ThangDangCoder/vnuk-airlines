<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>


<c:import url="/resources/jsp/header_booking.jsp" />
    <h3>Update plane  n� ${plane.id}</h3>

    <form action="../plane/update" method="post">
        <input type="hidden" name="id" value="${plane.id}" />
        Number Of Seat : <input type="text" name="numberOfSeat" value="${plane.numberOfSeat}"/>
        Plane Model Id: <br/>
        <form:select path = "plane.planeModelId">
        	<form:options items = "${planeModelList}" />
         </form:select>
        <br />
       
		<button type="submit" class="btn btn-success">
            <i class="fa fa-repeat" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Update
        </button>
        <a class="btn btn-default" href="../planes">
            <i class="fa fa-times-rectangle-o" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Cancel
        </a>
    </form>
<c:import url="/resources/jsp/body-close.jsp" />
