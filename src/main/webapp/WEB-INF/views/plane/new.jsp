<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>


<c:import url="/resources/jsp/header_booking.jsp" />
    <h3>Create Plane</h3>
	<form action="../plane/create" method="post">
        Number OF Seat :<input type="text" name="numberOfSeat" />
        <br />
        Plane Model Id: <br/>
        <form:select path = "plane.planeModelId">
        	<form:options items = "${planeModelList}" />
         </form:select>
        <br />
        <button type="submit" class="btn btn-success">
            <i class="fa fa-check" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Create
        </button>
        
        <a class="btn btn-default" href="../planes">
            <i class="fa fa-times-rectangle-o" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Cancel
        </a>
    </form>
<c:import url="/resources/jsp/body-close.jsp" />