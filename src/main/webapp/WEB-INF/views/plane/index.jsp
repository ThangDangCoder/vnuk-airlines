<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:import url="/resources/jsp/header_booking.jsp" />

    <a href="plane/new" class="btn btn-primary">
        <i class="fa fa-plus" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;New Plane
    </a>

    <br /><br />
    
    <div id="my-notice"></div>
    
    <table class="table table-bordered table-hover table-responsive table-striped">
        <thead>
            <tr>
                <th></th>
                <th>Id</th>
                <th>Number Of Seat</th>
                <th>Plane Model</th>
            </tr>
        </thead>

        <tbody>

            <c:forEach var="plane" items="${planes}">

                <tr>
                    <td>
                    	<a href="plane/show?id=${plane.id}" class="btn btn-xs btn-success" 
                           data-toggle="tooltip" title="Show plane" data-placement="bottom">
                            <i class="fa fa-eye" aria-hidden="true"></i>
                        </a>
                        
                        <a href="plane/edit?id=${plane.id}" class="btn btn-xs btn-primary" 
                           data-toggle="tooltip" title="Edit plane" data-placement="bottom">
                            <i class="fa fa-pencil" aria-hidden="true"></i>
                        </a>
                       
                       <%--    DELETING WITH AJAX   --%>

                        <button type="button"
                                class="btn btn-xs btn-danger my-plane-to-delete" 
                                value="${plane.id}"
                                data-toggle="tooltip" 
                                title="Delete plane" 
                                data-placement="bottom">
                            <i class="fa fa-trash" aria-hidden="true"></i>
                        </button>

                    </td>
                    
                    <td>${plane.id}</td>
                    <td>${plane.numberOfSeat}</td>
                    <td>${plane.planeModel.name}</td>
                    
               </tr>

            </c:forEach>

        </tbody>
    </table>
            
<c:import url="/resources/jsp/body-close-planes.jsp" />
