<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:import url="/resources/jsp/header_booking.jsp" />

    <a href="planeModel/new" class="btn btn-primary">
        <i class="fa fa-plus" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;New Plane Model
    </a>

    <br /><br />
    
    <div id="my-notice"></div>
    
    <table class="table table-bordered table-hover table-responsive table-striped">
        <thead>
            <tr>
                <th></th>
                <th>Id</th>
                <th>Name</th>
                <th>Plane Manufacture</th>
            </tr>
        </thead>

        <tbody>

            <c:forEach var="planeModel" items="${planeModels}">

                <tr>
                    <td>
                    
                   	 	<a href="planeModel/show?id=${planeModel.id}" class="btn btn-xs btn-success" 
                           data-toggle="tooltip" title="Show plane Model" data-placement="bottom">
                            <i class="fa fa-eye" aria-hidden="true"></i>
                        </a>
                        
                        <a href="planeModel/edit?id=${planeModel.id}" class="btn btn-xs btn-primary" 
                           data-toggle="tooltip" title="Edit planeModel" data-placement="bottom">
                            <i class="fa fa-pencil" aria-hidden="true"></i>
                        </a>
                       
                       <%--    DELETING WITH AJAX   --%>

                        <button type="button"
                                class="btn btn-xs btn-danger my-planeModel-to-delete" 
                                value="${planeModel.id}"
                                data-toggle="tooltip" 
                                title="Delete plane model" 
                                data-placement="bottom">
                            <i class="fa fa-trash" aria-hidden="true"></i>
                        </button>

                    </td>
                    
                    <td>${planeModel.id}</td>
                    <td>${planeModel.name}</td>
                    <td>${planeModel.planeManufacture.name}</td>
                    
               </tr>

            </c:forEach>

        </tbody>
    </table>
            
<c:import url="/resources/jsp/body-close-planeModels.jsp" />
