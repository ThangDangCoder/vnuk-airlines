<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>


<c:import url="/resources/jsp/header_booking.jsp" />
    <h3>Create country</h3>
	<form action="../planeModel/create" method="post">
        Plane Model :<input type="text" name="name" />
        <br />
        Plane Manufacture Id: <br/>
        <form:select path = "planeModel.planeManufactureId">
        	<form:options items = "${planeManufactureList}" />
        </form:select>
        <br />
        <button type="submit" class="btn btn-success">
            <i class="fa fa-check" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Create
        </button>
        
        <a class="btn btn-default" href="../planeModels">
            <i class="fa fa-times-rectangle-o" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Cancel
        </a>
    </form>
<c:import url="/resources/jsp/body-close.jsp" />