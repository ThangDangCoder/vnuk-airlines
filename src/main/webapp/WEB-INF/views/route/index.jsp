<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:import url="/resources/jsp/header_booking.jsp" />

    <a href="route/new" class="btn btn-primary">
        <i class="fa fa-plus" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;New Route
    </a>

    <br /><br />
    
    <div id="my-notice"></div>
    
    <table class="table table-bordered table-hover table-responsive table-striped">
        <thead>
            <tr>
                <th></th>
                <th>Id</th>
                <th>From AirPort</th>
                <th>To AirPort</th>
            </tr>
        </thead>

        <tbody>

            <c:forEach var="route" items="${routes}">

                <tr>
                    <td>
                    	<a href="route/show?id=${route.id}" class="btn btn-xs btn-success" 
                           data-toggle="tooltip" title="Show route" data-placement="bottom">
                            <i class="fa fa-eye" aria-hidden="true"></i>
                        </a>
                        
                        <a href="route/edit?id=${route.id}" class="btn btn-xs btn-primary" 
                           data-toggle="tooltip" title="Edit route" data-placement="bottom">
                            <i class="fa fa-pencil" aria-hidden="true"></i>
                        </a>
                       
                       <%--    DELETING WITH AJAX   --%>

                        <button type="button"
                                class="btn btn-xs btn-danger my-route-to-delete" 
                                value="${route.id}"
                                data-toggle="tooltip" 
                                title="Delete route" 
                                data-placement="bottom">
                            <i class="fa fa-trash" aria-hidden="true"></i>
                        </button>

                    </td>
                    
                    <td>${route.id}</td>
                    <td>${route.airportFrom.code}</td>
                    <td>${route.airportTo.code}</td>
                    
               </tr>

            </c:forEach>

        </tbody>
    </table>
            
<c:import url="/resources/jsp/body-close-routes.jsp" />
