<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>


<c:import url="/resources/jsp/header_booking.jsp" />
    <h3>Update city n� ${city.id}</h3>

    <form action="../city/update" method="post">
        <input type="hidden" name="id" value="${city.id}" />
        Name : <br />
        
        <input type="text" name="name" value="${city.name}" />
        <br /><br />

		Country ID : <br />
		
		 <form:select path = "city.countryId">
            <form:options items = "${countryList}" />
         </form:select>  
        <br /><br />
		<button type="submit" class="btn btn-success">
            <i class="fa fa-repeat" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Update
        </button>
        <a class="btn btn-default" href="../cities">
            <i class="fa fa-times-rectangle-o" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Cancel
        </a>
    </form>
<c:import url="/resources/jsp/body-close.jsp" />
