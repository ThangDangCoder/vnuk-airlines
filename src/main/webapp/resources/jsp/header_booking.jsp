<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:import url="/resources/jsp/body-open.jsp" />
<c:import url="/resources/jsp/header.jsp" />
<html>
	<body>
		<header id="gtco-header" class="gtco-cover gtco-cover-sm" role="banner">
			<div class="overlay"></div>
			<div class="gtco-container">
				<div class="row">
					<div class="col-md-12 col-md-offset-0 text-center">
						<div class="row row-mt-15em">
		
							<div class="col-md-12 mt-text animate-box" data-animate-effect="fadeInUp">
								<h1>Plans for Everyone</h1>	
							</div>
							
						</div>
					</div>
				</div>
			</div>
		</header>
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center gtco-heading">
					<h2>Choose The Best Plan For You</h2>
					<p>Join over 1 Million of users.</p>
				</div>
			</div>
		</div>