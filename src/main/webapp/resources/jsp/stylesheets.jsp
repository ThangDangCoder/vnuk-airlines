<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<link href="<c:url value="/resources/bootstrap/css/bootstrap.css" /> " type="text/css" rel="stylesheet" />
<link href="<c:url value="/resources/font-awesome/css/font-awesome.min.css" /> " type="text/css" rel="stylesheet" />

<link href="<c:url value="/resources/css/animate.css" /> " type="text/css" rel="stylesheet" />
<link href="<c:url value="/resources/css/bootstrap-datepicker.min.css" /> " type="text/css" rel="stylesheet" />
<link href="<c:url value="/resources/css/flexslider.css" /> " type="text/css" rel="stylesheet" />
<link href="<c:url value="/resources/css/icomoon.css" /> " type="text/css" rel="stylesheet" />
<link href="<c:url value="/resources/css/magnific-popup.css" /> " type="text/css" rel="stylesheet" />
<link href="<c:url value="/resources/css/owl.carousel.min.css" /> " type="text/css" rel="stylesheet" />
<link href="<c:url value="/resources/css/owl.theme.default.min.css" /> " type="text/css" rel="stylesheet" />
<link href="<c:url value="/resources/css/style.css" /> " type="text/css" rel="stylesheet" />
<link href="<c:url value="/resources/css/themify-icons.css" /> " type="text/css" rel="stylesheet" />

