<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

	<body>
		<div class="gtco-section">
			<div class="gtco-container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 text-center gtco-heading animate-box">
						<h2>Frequently Ask Questions</h2>
					</div>
				</div>
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<ul class="fh5co-faq-list">
							<li class="animate-box">
								<h2>What is Traveler?</h2>
								<p>Traveler is one who travels in any way. Distance is not material. A townsman or neighbor may be a traveler, and therefore a guest at an inn, as well as he who comes from a distance or from a foreign country.</p>
							</li>
							<li class="animate-box">
								<h2>What language are available?</h2>
								<p>Our service are available in 5 languages: English, Spanish, German, French, and Dutch.</p>
							</li>
							<li class="animate-box">
								<h2>How do I use Traveler features?</h2>
								<p>You can see which trip you want.</p>
							</li>
							<li class="animate-box">
								<h2>Is Traveler free??</h2>
								<p>Yes, you can use our service everywhere, everytime without costs.</p>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	
		
		<div id="gtco-subscribe">
			<div class="gtco-container">
				<div class="row animate-box">
					<div class="col-md-8 col-md-offset-2 text-center gtco-heading">
						<h2>Subscribe</h2>
						<p>Be the first to know about the new trips.</p>
					</div>
				</div>
				<div class="row animate-box">
					<div class="col-md-8 col-md-offset-2">
						<form class="form-inline">
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<label for="email" class="sr-only">Email</label>
									<input type="email" class="form-control" id="email" placeholder="Your Email">
								</div>
							</div>
							<div class="col-md-6 col-sm-6">
								<button type="submit" class="btn btn-default btn-block">Subscribe</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	
		<footer id="gtco-footer" role="contentinfo">
			<div class="gtco-container">
				<div class="row row-p	b-md">
	
					<div class="col-md-4">
						<div class="gtco-widget">
							<h3>About Us</h3>
							<p>We are TAT Technology from VNUK University.</p>
						</div>
					</div>
	
					<div class="col-md-2 col-md-push-1">
						<div class="gtco-widget">
							<h3>Destination</h3>
							<ul class="gtco-footer-links">
								<li><a href="#">Da Nang</a></li>
								<li><a href="#">Ha Noi</a></li>
								<li><a href="#">Sai gon</a></li>
								<li><a href="#">Can tho</a></li>
								<li><a href="#">Seoul</a></li>
								<li><a href="#">Busan</a></li>
							</ul>
						</div>
					</div>
	
					<div class="col-md-2 col-md-push-1">
						<div class="gtco-widget">
							<h3>Hotels</h3>
							<ul class="gtco-footer-links">
								<li><a href="#">Luxe Hotel</a></li>
								<li><a href="#">Italy 5 Star hotel</a></li>
								<li><a href="#">Dubai Hotel</a></li>
								<li><a href="#">Deluxe Hotel</a></li>
								<li><a href="#">BoraBora Hotel</a></li>
							</ul>
						</div>
					</div>
	
					<div class="col-md-3 col-md-push-1">
						<div class="gtco-widget">
							<h3>Get In Touch</h3>
							<ul class="gtco-quick-contact">
								<li><a href="#"><i class="icon-phone"></i> +84 915258593</a></li>
								<li><a href="#"><i class="icon-mail2"></i> tkpproo@gmail.com</a></li>
								<li><a href="#"><i class="icon-chat"></i> Live Chat</a></li>
							</ul>
						</div>
					</div>
	
				</div>
	
				<div class="row copyright">
					<div class="col-md-12">
						<p class="pull-right">
							<ul class="gtco-social-icons pull-right">
								<li><a href="#"><i class="icon-twitter"></i></a></li>
								<li><a href="#"><i class="icon-facebook"></i></a></li>
								<li><a href="#"><i class="icon-linkedin"></i></a></li>
								<li><a href="#"><i class="icon-dribbble"></i></a></li>
							</ul>
					</div>
				</div>
	
			</div>
		</footer>
	

	<div class="gototop js-top">
		<a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
	</div>
	</body>
		
</html>	
