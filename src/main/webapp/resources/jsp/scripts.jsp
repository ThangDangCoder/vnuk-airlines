<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<script type="text/javascript" src="<c:url value="/resources/js/jquery.js" /> "></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery-ui.js" /> "></script>
<script type="text/javascript" src="<c:url value="/resources/bootstrap/js/bootstrap.min.js" /> "></script>
<script type="text/javascript" src="<c:url value="/resources/js/app.js" /> "></script>

<script type="text/javascript" src="<c:url value="/resources/js/bootstrap-datepicker.min.js" /> "></script>
<script type="text/javascript" src="<c:url value="/resources/js/bootstrap.min.js" /> "></script>
<%-- <script type="text/javascript" src="<c:url value="/resources/js/google_map.js" /> "></script> --%>
<script type="text/javascript" src="<c:url value="/resources/js/main.js" /> "></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.stellar.min.js" /> "></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.waypoints.min.js" /> "></script>
<script type="text/javascript" src="<c:url value="/resources/js/magnific-popup-options.js" /> "></script>
<script type="text/javascript" src="<c:url value="/resources/js/owl.carousel.min.js" /> "></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.countTo.js" /> "></script>
<script type="text/javascript" src="<c:url value="/resources/js/modernizr-2.6.2.min.js" /> "></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.magnific-popup.min.js" /> "></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.easing.1.3.js" /> "></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.easypiechart.min.js" /> "></script>
<script type="text/javascript" src="<c:url value="/resources/js/respond.min.js" /> "></script>

