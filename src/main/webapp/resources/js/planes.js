
$(function(){

    //  DELETING TASK
    $('.my-plane-to-delete').on('click', function (e){
        e.preventDefault();

        var thisPlane = $(this);
        var planeId = thisPlane.val();

        $.post("plane/delete", {'id' : planeId}, function() {

        });
        
        $.ajax({
           
            type: "POST",
            url: "plane/delete",
            
            data: {
                id: planeId
            },
            
            success: function() {
                
            	thisPlane.closest('tr').remove();
                
                $('#my-notice').text('Plane ' + planeId + ' has successfully been deleted.')
                        .addClass('my-notice-green')
                        .removeClass('my-notice-red');

            },
            
            error: function() {
                $('#my-notice').text('Something went wrong with deletion of Plane ' + planeId)
                        .removeClass('my-notice-green')
                        .addClass('my-notice-red');
            }
            
        });
        
    });

});
