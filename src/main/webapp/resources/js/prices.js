
$(function(){

    //  DELETING TASK
    $('.my-price-to-delete').on('click', function (e){
        e.preventDefault();

        var thisPrice = $(this);
        var priceId = thisPrice.val();

        $.post("price/delete", {'id' : priceId}, function() {

        });
        
        $.ajax({
           
            type: "POST",
            url: "price/delete",
            
            data: {
                id: priceId
            },
            
            success: function() {
                
                thisPrice.closest('tr').remove();
                
                $('#my-notice').text('Price' + priceId + ' has successfully been deleted.')
                        .addClass('my-notice-green')
                        .removeClass('my-notice-red');

            },
            
            error: function() {
                $('#my-notice').text('Something went wrong with deletion of price ' + priceId)
                        .removeClass('my-notice-green')
                        .addClass('my-notice-red');
            }
            
        });
        
    });

});
