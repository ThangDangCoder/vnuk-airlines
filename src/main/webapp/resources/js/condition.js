
$(function(){

    //  DELETING TASK
    $('.my-condition-to-delete').on('click', function (e){
        e.preventDefault();

        var thisCondition = $(this);
        var conditionId = thisCondition.val();

        $.post("condition/delete", {'id' : conditionId}, function() {

        });
        
        $.ajax({
           
            type: "POST",
            url: "condition/delete",
            
            data: {
                id: conditionId
            },
            
            success: function() {
                
                thisCity.closest('tr').remove();
                
                $('#my-notice').text('Condition' + conditionId + ' has successfully been deleted.')
                        .addClass('my-notice-green')
                        .removeClass('my-notice-red');

            },
            
            error: function() {
                $('#my-notice').text('Something went wrong with deletion of condition ' + conditionId)
                        .removeClass('my-notice-green')
                        .addClass('my-notice-red');
            }
            
        });
        
    });

});
