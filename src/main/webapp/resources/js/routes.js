
$(function(){

    //  DELETING TASK
    $('.my-route-to-delete').on('click', function (e){
        e.preventDefault();

        var thisRoute = $(this);
        var routeId = thisRoute.val();

        $.post("route/delete", {'id' : routeId}, function() {

        });
        
        $.ajax({
           
            type: "POST",
            url: "route/delete",
            
            data: {
                id: routeId
            },
            
            success: function() {
                
            	thisRoute.closest('tr').remove();
                
                $('#my-notice').text('Route ' + routeId + ' has successfully been deleted.')
                        .addClass('my-notice-green')
                        .removeClass('my-notice-red');

            },
            
            error: function() {
                $('#my-notice').text('Something went wrong with deletion of Route ' + routeId)
                        .removeClass('my-notice-green')
                        .addClass('my-notice-red');
            }
            
        });
        
    });

});
