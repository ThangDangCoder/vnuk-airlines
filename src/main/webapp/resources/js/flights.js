
$(function(){

    //  DELETING TASK
    $('.my-flight-to-delete').on('click', function (e){
        e.preventDefault();

        var thisFlight = $(this);
        var flightId = thisFlight.val();

        $.post("flight/delete", {'id' : flightId}, function() {

        });
        
        $.ajax({
           
            type: "POST",
            url: "flight/delete",
            
            data: {
                id: flightId
            },
            
            success: function() {
                
            	thisFlight.closest('tr').remove();
                
                $('#my-notice').text('Flight' + flightId + ' has successfully been deleted.')
                        .addClass('my-notice-green')
                        .removeClass('my-notice-red');

            },
            
            error: function() {
                $('#my-notice').text('Something went wrong with deletion of flight ' + flightId)
                        .removeClass('my-notice-green')
                        .addClass('my-notice-red');
            }
            
        });
        
    });

});
