
$(function(){

    //  DELETING TASK
    $('.my-extra-to-delete').on('click', function (e){
        e.preventDefault();

        var thisExtra = $(this);
        var extraId = thisExtra.val();

        $.post("extra/delete", {'id' : extraId}, function() {

        });
        
        $.ajax({
           
            type: "POST",
            url: "extra/delete",
            
            data: {
                id: extraId
            },
            
            success: function() {
                
            	thisExtra.closest('tr').remove();
                
                $('#my-notice').text('Extra ' + extraId + ' has successfully been deleted.')
                        .addClass('my-notice-green')
                        .removeClass('my-notice-red');

            },
            
            error: function() {
                $('#my-notice').text('Something went wrong with deletion of extras ' + extraId)
                        .removeClass('my-notice-green')
                        .addClass('my-notice-red');
            }
            
        });
        
    });

});
