
$(function(){

    //  DELETING TASK
    $('.my-airport-to-delete').on('click', function (e){
        e.preventDefault();

        var thisAirport = $(this);
        var airportId = thisAirport.val();

        $.post("airport/delete", {'id' : airportId}, function() {

        });
        
        $.ajax({
           
            type: "POST",
            url: "airport/delete",
            
            data: {
                id: airportId
            },
            
            success: function() {
                
                thisAirport.closest('tr').remove();
                
                $('#my-notice').text('Airport' + airportId + ' has successfully been deleted.')
                        .addClass('my-notice-green')
                        .removeClass('my-notice-red');

            },
            
            error: function() {
                $('#my-notice').text('Something went wrong with deletion of airport ' + airportId)
                        .removeClass('my-notice-green')
                        .addClass('my-notice-red');
            }
            
        });
        
    });

});
