
$(function(){

    //  DELETING TASK
    $('.my-country-to-delete').on('click', function (e){
        e.preventDefault();

        var thisCountry = $(this);
        var countryId = thisCountry.val();

        $.post("country/delete", {'id' : countryId}, function() {

        });
        
        $.ajax({
           
            type: "POST",
            url: "country/delete",
            
            data: {
                id: countryId
            },
            
            success: function() {
                
                thisCountry.closest('tr').remove();
                
                $('#my-notice').text('Country ' + countryId + ' has successfully been deleted.')
                        .addClass('my-notice-green')
                        .removeClass('my-notice-red');

            },
            
            error: function() {
                $('#my-notice').text('Something went wrong with deletion of countries ' + countryId)
                        .removeClass('my-notice-green')
                        .addClass('my-notice-red');
            }
            
        });
        
    });

});
