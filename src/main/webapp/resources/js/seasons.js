
$(function(){

    //  DELETING TASK
    $('.my-season-to-delete').on('click', function (e){
        e.preventDefault();

        var thisSeason = $(this);
        var seasonId = thisSeason.val();

        $.post("season/delete", {'id' : seasonId}, function() {

        });
        
        $.ajax({
           
            type: "POST",
            url: "season/delete",
            
            data: {
                id: seasonId
            },
            
            success: function() {
                
                thisSeason.closest('tr').remove();
                
                $('#my-notice').text('Season ' + seasonId + ' has successfully been deleted.')
                        .addClass('my-notice-green')
                        .removeClass('my-notice-red');

            },
            
            error: function() {
                $('#my-notice').text('Something went wrong with deletion of seasons ' + seasonId)
                        .removeClass('my-notice-green')
                        .addClass('my-notice-red');
            }
            
        });
        
    });

});
