
$(function(){

    //  DELETING TASK
    $('.my-planeManufacture-to-delete').on('click', function (e){
        e.preventDefault();

        var thisPlaneManufacture = $(this);
        var planeManufactureId = thisPlaneManufacture.val();

        $.post("planeManufacture/delete", {'id' : planeManufactureId}, function() {

        });
        
        $.ajax({
           
            type: "POST",
            url: "planeManufacture/delete",
            
            data: {
                id: planeManufactureId
            },
            
            success: function() {
                
            	thisPlaneManufacture.closest('tr').remove();
                
                $('#my-notice').text('Plane Manufacture ' + planeManufactureId + ' has successfully been deleted.')
                        .addClass('my-notice-green')
                        .removeClass('my-notice-red');

            },
            
            error: function() {
                $('#my-notice').text('Something went wrong with deletion of Plane Manufacture ' + planeManufactureId)
                        .removeClass('my-notice-green')
                        .addClass('my-notice-red');
            }
            
        });
        
    });

});
