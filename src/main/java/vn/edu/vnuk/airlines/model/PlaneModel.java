package vn.edu.vnuk.airlines.model;

import javax.validation.constraints.NotNull;

public class PlaneModel{
	private long id;
	private String name;
	private long planeManufactureId;
	private PlaneManufacture planeManufacture;
	
	public PlaneManufacture getPlaneManufacture() {
		return planeManufacture;
	}
	public void setPlaneManufacture(PlaneManufacture planeManufacture) {
		this.planeManufacture = planeManufacture;
	}
	@NotNull
	public long getId(){
		return id;
		}
	public String getName(){
		return name;
		}
	public long getPlaneManufactureId(){
		return planeManufactureId;
		}
	
	
	public void setId(long id){
		this.id = id;
		}
	public void setName(String name){
		this.name = name;
		}
	public void setPlaneManufactureId(long planeManufactureId){
		this.planeManufactureId = planeManufactureId;
	}
}
