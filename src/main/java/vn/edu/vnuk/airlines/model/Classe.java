package vn.edu.vnuk.airlines.model;

import javax.validation.constraints.NotNull;

public class Classe{
	private long id;
	private String name;
	
	@NotNull
	public long getId(){
		return id;
		}
	
	public String getName(){
		return name;
		}
	public void setId(long id){
		this.id = id;
		}
	public void setName(String name){
		this.name = name;
		}
}