package vn.edu.vnuk.airlines.model;

import javax.validation.constraints.NotNull;

public class Route {
	private long id;
	private long fromAirPortId;
	private long toAirPortId;
	private Airport airportFrom;
	private Airport airportTo;
	
	
	public Airport getAirportFrom() {
		return airportFrom;
	}
	public void setAirportFrom(Airport airportFrom) {
		this.airportFrom = airportFrom;
	}
	public Airport getAirportTo() {
		return airportTo;
	}
	public void setAirportTo(Airport airportTo) {
		this.airportTo = airportTo;
	}
	public void setId(long id){
		this.id = id;
	}
	@NotNull
	public long getId(){
		return id;
	}
	
	public void setFromAirPortId(long fromAirPortId){
		this.fromAirPortId = fromAirPortId;
	}
	
	public long getFromAirPortId(){
		return fromAirPortId;
	}
	
	public void setToAirPortId(long toAirPortId){
		this.toAirPortId = toAirPortId;
	}
	
	public long getToAirPortId(){
		return toAirPortId;
	}
}
