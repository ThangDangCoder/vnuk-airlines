package vn.edu.vnuk.airlines.model;
import java.sql.Time;
import java.util.Date;

import javax.validation.constraints.NotNull;
public class Flight{
	
	private long id;
	private long routeId;
	private long planeId;
	private Date departureDate;
	private Date arrivalDate;
	private Time departureTime;
	private Time arrivalTime; 
	private String flightCode;
	private long dayId;
	
	private Plane plane;
	private Day day;
	private PlaneModel planeModel;
	private PlaneManufacture planeManufacture;
	private Route route;

	public Route getRoute() {
		return route;
	}
	public void setRoute(Route route) {
		this.route = route;
	}
	public PlaneManufacture getPlaneManufacture() {
		return planeManufacture;
	}
	public void setPlaneManufacture(PlaneManufacture planeManufacture) {
		this.planeManufacture = planeManufacture;
	}
	public PlaneModel getPlaneModel() {
		return planeModel;
	}
	public void setPlaneModel(PlaneModel planeModel) {
		this.planeModel = planeModel;
	}
	public Plane getPlane() {
		return plane;
	}
	public void setPlane(Plane plane) {
		this.plane = plane;
	}
	public Day getDay() {
		return day;
	}
	public void setDay(Day day) {
		this.day = day;
	}
	public void setId(long id){
		this.id = id;
	}
	@NotNull
	public long getId(){
		return id;
	}
	
	public void setRouteId(long routeId){
		this.routeId = routeId;
	}
	
	public long getRouteId(){
		return routeId;
	}
	
	public void setPlaneId(long planeId){
		this.planeId = planeId;
	}
	
	public long getPlaneId(){
		return planeId;
	}
	
	public void setDepartureTime(Time departureTime){
		this.departureTime = departureTime;
	}
	
	public Time getDepartureTime(){
		return departureTime;
	}
	
	public Time getArrivalTime() {
		return arrivalTime;
	}

	public void setArrivalTime(Time arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public String getFlightCode() {
		return flightCode;
	}

	public void setFlightCode(String flightCode) {
		this.flightCode = flightCode;
	}
	
	public void setDayId(long dayId) {
		this.dayId = dayId;
	}
	
	public long getDayId() {
		return dayId;
	}
	
	public Date getDepartureDate() {
		return departureDate;
	}
	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}
	public Date getArrivalDate() {
		return arrivalDate;
	}
	public void setArrivalDate(Date arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

}