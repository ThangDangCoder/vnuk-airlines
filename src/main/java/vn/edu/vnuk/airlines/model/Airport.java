package vn.edu.vnuk.airlines.model;

import javax.validation.constraints.NotNull;

public class Airport{
	
	private long id;
	private String code;
	private long cityId;
	private City city;
	
	@NotNull
	
	public long getId(){
		return id;
		}
	public City getCity() {
		return city;
	}
	public void setCity(City city) {
		this.city = city;
	}
	public String getCode(){
		return code;
		}
	public long getCityId(){
		return cityId;
		}
		
	public void setId(long id){
		this.id = id;
		}
	public void setCode(String code){
		this.code = code;
		}
	public void setcityId(long cityId){
		this.cityId = cityId;
		}
}