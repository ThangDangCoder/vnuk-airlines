package vn.edu.vnuk.airlines.model;

import javax.validation.constraints.NotNull;

public class PriceExtra {
	
	private long id;
	private long priceId;
	private long extraId;
	private boolean isIncludedPrice;
	private Price prices;
	private Extra extra;
	
	public Price getPrices() {
		return prices;
	}
	public void setPrice(Price price) {
		this.prices = price;
	}
	public Extra getExtra() {
		return extra;
	}
	public void setExtra(Extra extra) {
		this.extra = extra;
	}
	@NotNull
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getPriceId() {
		return priceId;
	}
	public void setPriceId(long priceId) {
		this.priceId = priceId;
	}
	public long getExtraId() {
		return extraId;
	}
	public void setExtraId(long extraId) {
		this.extraId = extraId;
	}
	public boolean isIsIncludedPrice() {
		return isIncludedPrice;
	}
	public void setIncludedPrice(boolean isIncludedPrice) {
		this.isIncludedPrice = isIncludedPrice;
	}
	
	
}
