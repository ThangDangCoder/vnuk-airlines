package vn.edu.vnuk.airlines.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.stereotype.Repository;

import vn.edu.vnuk.airlines.model.Condition;

@Repository
public class ConditionDao {
	   private Connection connection;

	   public void setDataSource(DataSource dataSource) {
	        
	        try {
	 	    	this.connection = dataSource.getConnection();
	 	    } catch (SQLException e) {
	 	    	throw new RuntimeException(e);
	 	    }
	        
	     }
	    
	    //  CREATE
	    public void create(Condition conditions) throws SQLException{

	        String sqlQuery = "insert into conditions (name,description) "
	                        +	"values (?, ?)";

	        PreparedStatement statement;

	        try {
	                statement = connection.prepareStatement(sqlQuery);

	                //	Replacing "?" through values
	                statement.setString(1, conditions.getName());
	                statement.setString(2, conditions.getDescription());


	                // 	Executing statement
	                statement.execute();

	                System.out.println("New record in DB !");

	        } catch (Exception e) {
	                // TODO Auto-generated catch block
	                e.printStackTrace();
	        } finally {
	                System.out.println("Done !");
	        }

	    }
	    
	    //  READ (List of Tasks)
	    @SuppressWarnings("finally")
	    public List<Condition> read() throws SQLException {

	        String sqlQuery = "select * from conditions";
	        PreparedStatement statement;
	        List<Condition> conditions = new ArrayList<Condition>();

	        try {

	            statement = connection.prepareStatement(sqlQuery);

	            // 	Executing statement
	            ResultSet results = statement.executeQuery();
	            
	            while(results.next()){

	                Condition condition = new Condition();
	                condition.setId(results.getLong("id"));
	                condition.setName(results.getString("name"));
	                condition.setDescription(results.getString("description"));
	                
	                conditions.add(condition);

	            }

	            results.close();
	            statement.close();


	        } catch (Exception e) {
	                // TODO Auto-generated catch block
	                e.printStackTrace();
	        } finally {
	                return conditions;
	        }
	    }
	    
	    
	    @SuppressWarnings({ "finally" })
		public Condition read(int id) throws SQLException{

	        String sqlQuery = "select * from conditions where id=?";

	        PreparedStatement statement;
	        Condition condition = new Condition();

	        try {
	            statement = connection.prepareStatement(sqlQuery);

	            //	Replacing "?" through values
	            statement.setLong(1, id);

	            // 	Executing statement
	            ResultSet results = statement.executeQuery();

	            if(results.next()){

	                condition.setId(results.getLong("id"));
	                condition.setName(results.getString("name"));
	                condition.setDescription(results.getString("description"));
	               

	                
	            statement.close();
	            }
	        } catch (Exception e) {
	                // TODO Auto-generated catch block
	                e.printStackTrace();
	        } finally {
	            
	            
	            return condition;
	        }

	    }
	    
	    //  UPDATE
	    public void update(Condition condition) throws SQLException {
	        String sqlQuery = "update conditions set name=?, description=?" 
	                            + "where id=?";
	        
	        try {
	            PreparedStatement statement = connection.prepareStatement(sqlQuery);
	            statement.setString(1, condition.getName());
	            statement.setString(2, condition.getDescription());
	            statement.setLong(3, condition.getId());
	            statement.execute();
	            statement.close();
	            
	            System.out.println("Conditions successfully modified.");
	        } 

	        catch (Exception e) {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
	        }
	    }
	    
	//  DELETE
	    public void delete(int id) throws SQLException {
	        String sqlQuery = "delete from conditions where id=?";

	        try {
	            PreparedStatement statement = connection.prepareStatement(sqlQuery);
	            statement.setLong(1, id);
	            statement.execute();
	            statement.close();
	            
	            System.out.println("Conditions successfully deleted.");

	        } 

	        catch (Exception e) {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
	        }
	        
	    }
}
