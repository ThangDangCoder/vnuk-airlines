package vn.edu.vnuk.airlines.dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.stereotype.Repository;

import vn.edu.vnuk.airlines.model.Day;

@Repository
public class DayDao {
	 private Connection connection;

	    public void setDataSource(DataSource dataSource) {
	        
	        try {
	 	    	this.connection = dataSource.getConnection();
	 	    } catch (SQLException e) {
	 	    	throw new RuntimeException(e);
	 	    }
	        
	     }

	    //  CREATE
	    public void create(Day day) throws SQLException{

	        String sqlQuery = "insert into days (name) "
	                        +	"values (?)";

	        PreparedStatement statement;

	        try {
	                statement = connection.prepareStatement(sqlQuery);

	                //	Replacing "?" through values
	                statement.setString(1, day.getName());


	                // 	Executing statement
	                statement.execute();

	                System.out.println("New record in DB !");

	        } catch (Exception e) {
	                // TODO Auto-generated catch block
	                e.printStackTrace();
	        } finally {
	                System.out.println("Done !");
	        }

	    }
	    
	    //  READ (List of Tasks)
	    @SuppressWarnings("finally")
	    public List<Day> read() throws SQLException {

	        String sqlQuery = "select * from days";
	        PreparedStatement statement;
	        List<Day> days = new ArrayList<Day>();

	        try {

	            statement = connection.prepareStatement(sqlQuery);

	            // 	Executing statement
	            ResultSet results = statement.executeQuery();
	            
	            while(results.next()){

	                Day day = new Day();
	                day.setId(results.getLong("id"));
	                day.setName(results.getString("name"));
	               
	                days.add(day);
	            }

	            results.close();
	            statement.close();


	        } catch (Exception e) {
	                // TODO Auto-generated catch block
	                e.printStackTrace();
	        } finally {
	                return days;
	        }
	    }
	    
	    
	    
	    @SuppressWarnings({ "finally"})
		public Day read(int id) throws SQLException{

	        String sqlQuery = "select * from days where id=?";

	        PreparedStatement statement;
	        Day day = new Day();

	        try {
	            statement = connection.prepareStatement(sqlQuery);

	            //	Replacing "?" through values
	            statement.setLong(1, id);

	            // 	Executing statement
	            ResultSet results = statement.executeQuery();

	            if(results.next()){

	                day.setId(results.getLong("id"));
	                day.setName(results.getString("name"));
	               

	                
	            statement.close();
	            }
	        } catch (Exception e) {
	                // TODO Auto-generated catch block
	                e.printStackTrace();
	        } finally {
	            
	            return day;
	        }

	    }
	    
	    //  UPDATE
	    public void update(Day day) throws SQLException {
	        String sqlQuery = "update days set name=?" 
	                            + "where id=?";
	        
	        try {
	            PreparedStatement statement = connection.prepareStatement(sqlQuery);
	            statement.setString(1, day.getName());
	            statement.setLong(2, day.getId());
	            statement.execute();
	            statement.close();
	            
	            System.out.println("days successfully modified.");
	        } 

	        catch (Exception e) {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
	        }
	        
	    }
	    
	    //  DELETE
	    public void delete(int id) throws SQLException {
	        String sqlQuery = "delete from days where id=?";

	        try {
	            PreparedStatement statement = connection.prepareStatement(sqlQuery);
	            statement.setLong(1, id);
	            statement.execute();
	            statement.close();
	            
	            System.out.println("days successfully deleted.");

	        } 

	        catch (Exception e) {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
	        }

	    }
	    
	    
	    
}
