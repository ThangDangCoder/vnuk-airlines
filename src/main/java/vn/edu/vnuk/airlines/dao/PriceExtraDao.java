package vn.edu.vnuk.airlines.dao;

import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.stereotype.Repository;

import vn.edu.vnuk.airlines.model.Extra;
import vn.edu.vnuk.airlines.model.Price;
import vn.edu.vnuk.airlines.model.PriceExtra;


@Repository
public class PriceExtraDao {

	private Connection connection;
	
	public void setDataSource(DataSource dataSource) {
        
        try {
 	    	this.connection = dataSource.getConnection();
 	    } catch (SQLException e) {
 	    	throw new RuntimeException(e);
 	    }
    }
	
	// CREATE
    public void create(PriceExtra priceExtras) throws SQLException{

        String sqlQuery = "insert into prices_extras (price_id, extra_id, is_included_price)  "
                        +	"values (?, ?, ?)";

        PreparedStatement statement;

        try {
                statement = connection.prepareStatement(sqlQuery);

                //	Replacing "?" through values
                statement.setLong(1, priceExtras.getPriceId());
                statement.setLong(2, priceExtras.getExtraId());
                statement.setBoolean(3, priceExtras.isIsIncludedPrice());
               

                // 	Executing statement
                statement.execute();

                System.out.println("New record in DB !");

        } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
        } finally {
                System.out.println("Done !");
        }

    }
    
    //  READ (List of Routes)
    @SuppressWarnings("finally")
    public List<PriceExtra> read() throws SQLException {

        String sqlQuery = "select pe.price_id as price_id, pe.extra_id as extra_id, pe.is_included_price as is_include, "
        				+ "p.price as prices_price, e.name as extra_name "
        				+ "from prices_extras pe, prices p, extras e "
        				+ "where pe.price_id = p.id and pe.extra_id = e.id;";
        PreparedStatement statement;
        List<PriceExtra> priceExtras = new ArrayList<PriceExtra>();

        try {

            statement = connection.prepareStatement(sqlQuery);

            // 	Executing statement
            ResultSet results = statement.executeQuery();
            
            while(results.next()){

                PriceExtra priceExtra = new PriceExtra();
                Price price = new Price();
                Extra extra = new Extra();
                
                priceExtra.setId(results.getLong("price_id"));
                priceExtra.setIncludedPrice(results.getBoolean("is_include"));
                priceExtra.setPrice(price);
                priceExtra.setExtra(extra);
                
                price.setPrice(results.getDouble("prices_price"));
                
                extra.setName(results.getString("extra_name"));
                
                priceExtras.add(priceExtra);

            }

            results.close();
            statement.close();


        } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
        } finally {
               
                return priceExtras;
        }


    }
    //Read single 

    
    @SuppressWarnings({ "finally" })
	public PriceExtra read(int id) throws SQLException{

        String sqlQuery = "select pe.price_id as price_id, pe.extra_id as extra_id, pe.is_included_price as is_include, "
        		+ "p.price as prices_price, e.name as extra_name "
        		+ "from prices_extras pe, prices p, extras e where pe.id=? and pe.price_id = p.id and pe.extra_id = e.id;";

        PreparedStatement statement;
        PriceExtra priceExtra = new PriceExtra();
        Price price = new Price();
        Extra extra = new Extra();

        try {
            statement = connection.prepareStatement(sqlQuery);

            //	Replacing "?" through values
            statement.setLong(1, id);

            // 	Executing statement
            ResultSet results = statement.executeQuery();

            if(results.next()){

            	 priceExtra.setId(results.getLong("price_id"));
                 priceExtra.setIncludedPrice(results.getBoolean("is_include"));
                 priceExtra.setPrice(price);
                 priceExtra.setExtra(extra);
                 
                 price.setPrice(results.getDouble("prices_price"));
                 
                 extra.setName(results.getString("extra_name"));
              

            statement.close();

        } }catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
        } finally {
            

            
            return priceExtra;
        }

    }
//  UPDATE
    public void update(PriceExtra priceExtra) throws SQLException {
        String sqlQuery = "update prices_extras set price_id=?, extra_id=?, is_included_price=?" 
        		 		+" where id=?";
        
        try {
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setLong(1, priceExtra.getPriceId());
            statement.setLong(2, priceExtra.getExtraId());
            statement.setBoolean(3, priceExtra.isIsIncludedPrice());
            statement.setLong(4, priceExtra.getId());
            statement.execute();
            statement.close();
            
            System.out.println("PriceExtras successfully modified.");
        } 

        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        finally {
            
        }
        
    }
    
//  DELETE
    public void delete(int id) throws SQLException {
        String sqlQuery = "delete from prices_extras where id=?";

        try {
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setLong(1, id);
            statement.execute();
            statement.close();
            
            System.out.println("PriceExtras successfully deleted.");

        } 

        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }
}
