package vn.edu.vnuk.airlines.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.stereotype.Repository;

import vn.edu.vnuk.airlines.model.PlaneManufacture;

@Repository
public class PlaneManufactureDao {
	private Connection connection;

	 public void setDataSource(DataSource dataSource) {
	        
	        try {
	 	    	this.connection = dataSource.getConnection();
	 	    } catch (SQLException e) {
	 	    	throw new RuntimeException(e);
	 	    }
	        
	     }
    
//  CREATE
    public void create(PlaneManufacture planeManufactures) throws SQLException{

        String sqlQuery = "insert into plane_manufactures (name) "
                        +	"values (?)";

        PreparedStatement statement;

        try {
                statement = connection.prepareStatement(sqlQuery);

                //	Replacing "?" through values
                statement.setString(1, planeManufactures.getName());


                // 	Executing statement
                statement.execute();

                System.out.println("New record in DB !");

        } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
        } finally {
                System.out.println("Done !");
        }

    }
    
//  READ (List of Tasks)
    @SuppressWarnings("finally")
    public List<PlaneManufacture> read() throws SQLException {

        String sqlQuery = "select * from plane_manufactures";
        PreparedStatement statement;
        List<PlaneManufacture> planeManufactures = new ArrayList<PlaneManufacture>();

        try {

            statement = connection.prepareStatement(sqlQuery);

            // 	Executing statement
            ResultSet results = statement.executeQuery();
            
            while(results.next()){

                PlaneManufacture planeManufacture  = new PlaneManufacture();
                planeManufacture .setId(results.getLong("id"));
                planeManufacture .setName(results.getString("name"));
               
                planeManufactures.add(planeManufacture );

            }

            results.close();
            statement.close();


        } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
        } finally {
                return planeManufactures;
        }
    }
    
    @SuppressWarnings({ "finally" })
	public PlaneManufacture read(int id) throws SQLException{

        String sqlQuery = "select * from plane_manufactures where id=?";

        PreparedStatement statement;
        PlaneManufacture planeManufacture = new PlaneManufacture();

        try {
            statement = connection.prepareStatement(sqlQuery);

            //	Replacing "?" through values
            statement.setLong(1, id);

            // 	Executing statement
            ResultSet results = statement.executeQuery();

            if(results.next()){

                planeManufacture.setId(results.getLong("id"));
                planeManufacture.setName(results.getString("name"));
               
                statement.close();
            }
        } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
        } finally {
            return planeManufacture;
        }

    }
    
//  UPDATE
    public void update(PlaneManufacture planeManufacture) throws SQLException {
        String sqlQuery = "update plane_manufactures set name=? " 
                            + "where id=?";
        
        try {
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setString(1, planeManufacture.getName());
            statement.setLong(2, planeManufacture.getId());
            statement.execute();
            statement.close();
            
            System.out.println("plane_manufactures successfully modified.");
        } 

        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
    }
    
//  DELETE
    public void delete(int id) throws SQLException {
        String sqlQuery = "delete from plane_manufactures where id=?";

        try {
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setLong(1, id);
            statement.execute();
            statement.close();
            
            System.out.println("plane_manufactures successfully deleted.");

        } 

        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        

    }
}
