package vn.edu.vnuk.airlines.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.stereotype.Repository;

import vn.edu.vnuk.airlines.model.Extra;


@Repository
public class ExtraDao {

	private Connection connection;

	 public void setDataSource(DataSource dataSource) {
	        
	        try {
	 	    	this.connection = dataSource.getConnection();
	 	    } catch (SQLException e) {
	 	    	throw new RuntimeException(e);
	 	    }
   }
	 
	 public void create(Extra extras) throws SQLException{

	        String sqlQuery = "insert into extras (name)"
	                        +	"values (?)";

	        PreparedStatement statement;

	        try {
	                statement = connection.prepareStatement(sqlQuery);

	                //	Replacing "?" through values
	                statement.setString(1, extras.getName());
	                
	               

	                // 	Executing statement
	                statement.execute();

	                System.out.println("New record in DB !");

	        } catch (Exception e) {
	                // TODO Auto-generated catch block
	                e.printStackTrace();
	        } finally {
	                System.out.println("Done !");
	                
	        }

	    }
	 //  READ (List of PlaneModels)
	    @SuppressWarnings("finally")
	    public List<Extra> read() throws SQLException {

	        String sqlQuery = "select * from extras";
	        PreparedStatement statement;
	        List<Extra> extras = new ArrayList<Extra>();

	        try {

	            statement = connection.prepareStatement(sqlQuery);

	            // 	Executing statement
	            ResultSet results = statement.executeQuery();
	            
	            while(results.next()){

	                Extra extra = new Extra();
	                extra.setId(results.getLong("id"));
	                extra.setName(results.getString("name"));
	                

	                
	                extras.add(extra);

	            }

	            results.close();
	            statement.close();


	        } catch (Exception e) {
	                // TODO Auto-generated catch block
	                e.printStackTrace();
	        } finally {
	                
	                return extras;
	        }


	    }
	    //Read single 
	    
	    
	    @SuppressWarnings({ "finally" })
		public Extra read(int id) throws SQLException{

	        String sqlQuery = "select * from extras where id=?";

	        PreparedStatement statement;
	        Extra extra = new Extra();

	        try {
	            statement = connection.prepareStatement(sqlQuery);

	            //	Replacing "?" through values
	            statement.setLong(1, id);

	            // 	Executing statement
	            ResultSet results = statement.executeQuery();

	            if(results.next()){

	            	extra.setId(results.getLong("id"));
	            	extra.setName(results.getString("name"));
	                

	              

	            statement.close();

	        } }catch (Exception e) {
	                // TODO Auto-generated catch block
	                e.printStackTrace();
	        } finally {
	        	return extra;
	        }

	  }
	//  UPDATE
	    public void update(Extra extras) throws SQLException {
	        String sqlQuery = "update extras set name=? where id=?";
	        
	        try {
	            PreparedStatement statement = connection.prepareStatement(sqlQuery);
	            statement.setString(1, extras.getName());
	            statement.setLong(2, extras.getId());
	            statement.execute();
	            statement.close();
	            
	            System.out.println("Extras successfully modified.");
	        } 

	        catch (Exception e) {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
	        }
	        

	        
	    }
	    
	//  DELETE
	    public void delete(int id) throws SQLException {
	        String sqlQuery = "delete from extras where id=?";

	        try {
	            PreparedStatement statement = connection.prepareStatement(sqlQuery);
	            statement.setLong(1, id);
	            statement.execute();
	            statement.close();
	            
	            System.out.println("Extra successfully deleted.");

	        } 

	        catch (Exception e) {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
	        }

	    }
}
