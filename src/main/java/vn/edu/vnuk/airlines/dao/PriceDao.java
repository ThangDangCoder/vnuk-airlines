package vn.edu.vnuk.airlines.dao;

import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.stereotype.Repository;

import vn.edu.vnuk.airlines.model.Classe;
import vn.edu.vnuk.airlines.model.Condition;
import vn.edu.vnuk.airlines.model.Flight;
import vn.edu.vnuk.airlines.model.Price;
import vn.edu.vnuk.airlines.model.Season;


@Repository
public class PriceDao {

	private Connection connection;

	 public void setDataSource(DataSource dataSource) {
	        
	        try {
	 	    	this.connection = dataSource.getConnection();
	 	    } catch (SQLException e) {
	 	    	throw new RuntimeException(e);
	 	    }
	    }
	 
	 // CREATE
	    public void create(Price prices) throws SQLException{

	        String sqlQuery = "insert into prices (flight_id, class_id, season_id, condition_id, price)  "
	                        +	"values (?, ?, ?, ?, ?)";

	        PreparedStatement statement;

	        try {
	                statement = connection.prepareStatement(sqlQuery);

	                //	Replacing "?" through values
	                statement.setLong(1, prices.getFlightId());
	                statement.setLong(2, prices.getClassId());
	                statement.setLong(3, prices.getSeasonId());
	                statement.setLong(4, prices.getConditionId());
	                statement.setDouble(5, prices.getPrice());
	               

	                // 	Executing statement
	                statement.execute();

	                System.out.println("New record in DB !");

	        } catch (Exception e) {
	                // TODO Auto-generated catch block
	                e.printStackTrace();
	        } finally {
	                System.out.println("Done !");
	        }

	    }
	//  READ (List of PlaneModels)
	    @SuppressWarnings("finally")
	    public List<Price> read() throws SQLException {

	        String sqlQuery = "select p.id as price_id, p.price as prices_price, "
	        		+ "f.flight_code as flightCode, c.name as class_name, s.name as season_name, cd.description as condition_description "
	        		+ " from prices p, flights f, classes c, seasons s, conditions cd "
	        		+ " where p.flight_id = f.id and p.class_id = c.id and p.season_id = s.id and p.condition_id = cd.id;";
	        PreparedStatement statement;
	        List<Price> prices = new ArrayList<Price>();

	        try {

	            statement = connection.prepareStatement(sqlQuery);

	            // 	Executing statement
	            ResultSet results = statement.executeQuery();
	            
	            while(results.next()){

	                Price price = new Price();
	                Flight flight = new Flight();
	                Classe classe = new Classe();
	                Season season = new Season();
	                Condition condition = new Condition();
	                
	                
	                price.setId(results.getLong("price_id"));
	                price.setPrice(results.getDouble("prices_price"));
	                price.setClasse(classe);
	                price.setCondition(condition);
	                price.setSeason(season);
	                price.setFlight(flight);
	                
	                flight.setFlightCode(results.getString("flightCode"));
	                
	                classe.setName(results.getString("class_name"));
	                
	                season.setName(results.getString("season_name"));
	                
	                condition.setDescription(results.getString("condition_description"));
	                
	                prices.add(price);

	            }

	            results.close();
	            statement.close();


	        } catch (Exception e) {
	                // TODO Auto-generated catch block
	                e.printStackTrace();
	        } finally {
	               
	                return prices;
	        }


	    }
	    
	    //Read single 

	    
	    @SuppressWarnings({ "finally" })
		public Price read(int id) throws SQLException{

	        String sqlQuery = "select p.id as price_id, p.price as prices_price, f.flight_code as flightCode, "
	        		+ " c.name as class_name, s.name as season_name, cd.name as condition_name "
	        		+ " from prices p, flights f, classes c, seasons s, conditions cd where f.id=? and p.class_id = c.id and p.season_id = s.id and p.condition_id = cd.id;";

	        PreparedStatement statement;
	        Price price = new Price();
            Flight flight = new Flight();
            Classe classe = new Classe();
            Season season = new Season();
            Condition condition = new Condition();

	        try {
	            statement = connection.prepareStatement(sqlQuery);

	            //	Replacing "?" through values
	            statement.setLong(1, id);

	            // 	Executing statement
	            ResultSet results = statement.executeQuery();

	            if(results.next()){

	            	price.setId(results.getLong("price_id"));
	                price.setPrice(results.getDouble("prices_price"));
	                price.setClasse(classe);
	                price.setCondition(condition);
	                price.setSeason(season);
	                price.setFlight(flight);
	                
	                flight.setFlightCode(results.getString("flightCode"));
	                
	                classe.setName(results.getString("class_name"));
	                
	                season.setName(results.getString("season_name"));
	                
	                condition.setName(results.getString("condition_name"));

	              

	            statement.close();

	        } }catch (Exception e) {
	                // TODO Auto-generated catch block
	                e.printStackTrace();
	        } finally {
	            

	            
	            return price;
	        }

	    
	    }
	//  UPDATE
	    public void update(Price price) throws SQLException {
	        String sqlQuery = "update prices set flight_id=?, class_id=?, season_id=?, condition_id=?, price=?" 
	        		 		+" where id=?";
	        
	        try {
	            PreparedStatement statement = connection.prepareStatement(sqlQuery);
	            statement.setLong(1, price.getFlightId());
                statement.setLong(2, price.getClassId());
                statement.setLong(3, price.getSeasonId());
                statement.setLong(4, price.getConditionId());
                statement.setDouble(5, price.getPrice());
                statement.setLong(6, price.getId());
	            statement.execute();
	            statement.close();
	            
	            System.out.println("prices successfully modified.");
	        } 

	        catch (Exception e) {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
	        }
	        
	        finally {
	            
	        }
	        
	    }
	    
	//  DELETE
	    public void delete(int id) throws SQLException {
	        String sqlQuery = "delete from prices where id=?";

	        try {
	            PreparedStatement statement = connection.prepareStatement(sqlQuery);
	            statement.setLong(1, id);
	            statement.execute();
	            statement.close();
	            
	            System.out.println("Prices successfully deleted.");

	        } 

	        catch (Exception e) {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
	        }

	    }
	    
}
