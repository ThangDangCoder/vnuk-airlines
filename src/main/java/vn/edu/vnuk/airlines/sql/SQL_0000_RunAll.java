package vn.edu.vnuk.airlines.sql;


import java.sql.Connection;
import java.sql.SQLException;

import vn.edu.vnuk.airlines.jdbc.ConnectionFactory;


public class SQL_0000_RunAll {

	public static void main(String[] args) throws SQLException {
		
		/* Drop and create database */
		Connection connectionDb = new ConnectionFactory().getConnection("jdbc:mysql://localhost/");
		
		new SQL_0001_DropDatabase(connectionDb).run();
		new SQL_0005_CreateDatabase(connectionDb).run();

		connectionDb.close();
		
		/* Create all tables */
		Connection connectionTable = new ConnectionFactory().getConnection("jdbc:mysql://localhost/vnuk_airlines");
		
		new SQL_0010_CreateExtras(connectionTable).run();
		new SQL_0020_CreateConditions(connectionTable).run();
		new SQL_0030_CreateSeasons(connectionTable).run();
		new SQL_0040_CreatePlaneManufactures(connectionTable).run();
		new SQL_0050_CreateClasses(connectionTable).run();
		new SQL_0060_CreatePlaneModels(connectionTable).run();
		new SQL_0070_CreateCountries(connectionTable).run();
		new SQL_0080_CreateCities(connectionTable).run();
		new SQL_0090_CreateAirports(connectionTable).run();
		new SQL_0100_CreatePlanes(connectionTable).run();
		new SQL_0110_CreateRoutes(connectionTable).run();
		new SQL_0115_CreateDays(connectionTable).run();
		new SQL_0120_CreateFlights(connectionTable).run();
		new SQL_0130_CreatePrices(connectionTable).run();
		new SQL_0140_CreatePricesExtras(connectionTable).run();
		
		/* Create View */
		new SQL_0125_CreateViewPlane(connectionTable).run();
		
		/* Delete all datas from tables from (4010 - 4140)*/
		
		
		
		/*Insert data for tables*/
		new SQL_5010_InsertExtras(connectionTable).run();
		new SQL_5020_InsertConditions(connectionTable).run();
		new SQL_5030_InsertSeasons(connectionTable).run();
		new SQL_5040_InsertPlaneManufactures(connectionTable).run();
		new SQL_5050_InsertClasses(connectionTable).run();
		new SQL_5060_InsertPlaneModels(connectionTable).run();
		new SQL_5070_InsertCountries(connectionTable).run();
		new SQL_5080_InsertCities(connectionTable).run();
		new SQL_5090_InsertAirports(connectionTable).run();
		new SQL_5100_InsertPlanes(connectionTable).run();
		new SQL_5115_InsertDays(connectionTable).run();
		new SQL_5110_InsertRoutes(connectionTable).run();
		new SQL_5120_InsertFlights(connectionTable).run();
		new SQL_5130_InsertPrices(connectionTable).run();
		new SQL_5140_InsertPricesExtra(connectionTable).run();
		connectionTable.close();
    }

}
