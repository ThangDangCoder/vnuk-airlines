package vn.edu.vnuk.airlines.sql;

import java.sql.SQLException;

import java.sql.Connection;



public class SQL_5010_InsertExtras {

	private final Connection connection;
	private final String sqlQuery;
	
	public SQL_5010_InsertExtras(Connection connection) {
		super();
		this.connection = connection;
		this.sqlQuery = "insert into extras (name) "
				+ "values ('luggage'), ('pet'), ('meal')";
	}
	
	public void run() throws SQLException {

			try {
			        connection.prepareStatement(sqlQuery).execute();
			        System.out.println("New data in table extras in DB !");
			
			} catch (Exception e) {
			        // TODO Auto-generated catch block
			        e.printStackTrace();
			} finally {
			        System.out.println("Done !");
			        //connection.close();
			}
			
	}
	
	
}
