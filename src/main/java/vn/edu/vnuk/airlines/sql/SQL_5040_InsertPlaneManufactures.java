package vn.edu.vnuk.airlines.sql;

import java.sql.Connection;
import java.sql.SQLException;



public class SQL_5040_InsertPlaneManufactures {

	private final Connection connection;
	private final String sqlQuery;
	
	public SQL_5040_InsertPlaneManufactures(Connection connection) {
		super();
		this.connection = connection;
		this.sqlQuery = "insert into plane_manufactures (name) "
				+ "values ('Boeing'), ('Airbus')";
	}
	
	public void run() throws SQLException {

			try {
			        connection.prepareStatement(sqlQuery).execute();
			        System.out.println("New data in table plane_manufactures in DB !");
			
			} catch (Exception e) {
			        // TODO Auto-generated catch block
			        e.printStackTrace();
			} finally {
			        System.out.println("Done !");
			        //connection.close();
			}
			
	}
	
	
}
