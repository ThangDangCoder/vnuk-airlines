package vn.edu.vnuk.airlines.sql;

import java.sql.Connection;
import java.sql.SQLException;

public class SQL_0040_CreatePlaneManufactures {
	
	private final Connection connection;
	private final String sqlQuery;
	
	public SQL_0040_CreatePlaneManufactures(Connection connection) {
//		super();
		this.connection = connection;
		this.sqlQuery = "create table IF NOT EXISTS plane_manufactures ("
						+ "id BIGINT NOT NULL AUTO_INCREMENT, "
						+ "name VARCHAR(100), "
						+ "primary key (id)"
						+ ");";
	}
	
	public void run() throws SQLException {

			try {
			        connection.prepareStatement(sqlQuery).execute();
			        System.out.println("New table plane_manufactures in DB !");
			
			} catch (Exception e) {
			        // TODO Auto-generated catch block
			        e.printStackTrace();
			} finally {
			        System.out.println("Done !");
			        //connection.close();
			}
			
		}
	
}
