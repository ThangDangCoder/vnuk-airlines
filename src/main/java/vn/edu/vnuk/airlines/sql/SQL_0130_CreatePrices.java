package vn.edu.vnuk.airlines.sql;

import java.sql.Connection;
import java.sql.SQLException;

public class SQL_0130_CreatePrices {
	
	private final Connection connection;
	private final String sqlQuery;
	
	public SQL_0130_CreatePrices(Connection connection) {
		super();
		this.connection = connection;
		this.sqlQuery = "create table IF NOT EXISTS prices ("
						+ "id BIGINT NOT NULL AUTO_INCREMENT, "
						+ "flight_id BIGINT NOT NULL, "
						+ "class_id BIGINT NOT NULL, "
						+ "season_id BIGINT NOT NULL, "
						+ "condition_id BIGINT NOT NULL, "
						+ "price DOUBLE PRECISION, "
						+ "primary key (id), "
						+ "foreign key (flight_id) "
						+ "REFERENCES flights(id), "
						+ "foreign key (class_id) "
						+ "REFERENCES classes(id), "
						+ "foreign key (season_id) "
						+ "REFERENCES seasons(id), "
						+ "foreign key (condition_id) "
						+ "REFERENCES conditions(id)"
						+ ");";
	}
	
	public void run() throws SQLException {

			try {
			        connection.prepareStatement(sqlQuery).execute();
			        System.out.println("New table prices in DB !");
			
			} catch (Exception e) {
			        // TODO Auto-generated catch block
			        e.printStackTrace();
			} finally {
			        System.out.println("Done !");
			        //connection.close();
			}
			
		}
	
}
