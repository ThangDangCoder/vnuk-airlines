package vn.edu.vnuk.airlines.sql;

import java.sql.Connection;
import java.sql.SQLException;



public class SQL_5020_InsertConditions {

	private final Connection connection;
	private final String sqlQuery;
	
	public SQL_5020_InsertConditions(Connection connection) {
		super();
		this.connection = connection;
		this.sqlQuery = "insert into conditions (name, description) "
				+ "values ('Book','3 months earlier'), ('Book','within a week'), ('Book','in flight day')";
	}
	
	public void run() throws SQLException {

			try {
			        connection.prepareStatement(sqlQuery).execute();
			        System.out.println("New data in table conditions in DB !");
			
			} catch (Exception e) {
			        // TODO Auto-generated catch block
			        e.printStackTrace();
			} finally {
			        System.out.println("Done !");
			        //connection.close();
			}
			
	}
	
	
}
