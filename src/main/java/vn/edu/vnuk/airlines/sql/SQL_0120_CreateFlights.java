package vn.edu.vnuk.airlines.sql;

import java.sql.Connection;
import java.sql.SQLException;

public class SQL_0120_CreateFlights {
	
	private final Connection connection;
	private final String sqlQuery;
	
	public SQL_0120_CreateFlights(Connection connection) {
		super();
		this.connection = connection;
		this.sqlQuery = "create table IF NOT EXISTS flights ("
						+ "id BIGINT NOT NULL AUTO_INCREMENT, "
						+ "route_id BIGINT NOT NULL, "
						+ "plane_id BIGINT NOT NULL, "
						+ "day_id BIGINT NOT NULL, "
						+ "departure_time DATETIME NOT NULL, "
						+ "arrival_time DATETIME NOT NULL, "
						+ "flight_code VARCHAR(10), "
						+ "primary key (id), "
						+ "foreign key (route_id) "
						+ "REFERENCES routes(id), "
						+ "foreign key (day_id) "
						+ "REFERENCES days(id), "
						+ "foreign key (plane_id) "
						+ "REFERENCES planes(id)"
						+ ");";
	}
	
	public void run() throws SQLException {

			try {
			        connection.prepareStatement(sqlQuery).execute();
			        System.out.println("New table flights in DB !");
			
			} catch (Exception e) {
			        // TODO Auto-generated catch block
			        e.printStackTrace();
			} finally {
			        System.out.println("Done !");
			        //connection.close();
			}
			
		}
	
}
