package vn.edu.vnuk.airlines.sql;

import java.sql.Connection;
import java.sql.SQLException;

public class SQL_5140_InsertPricesExtra {

	private final Connection connection;
	private final String sqlQuery;
	
	public SQL_5140_InsertPricesExtra(Connection connection) {

		this.connection = connection;
		this.sqlQuery = "insert into prices_extras (price_id,extra_id,is_included_price) "
				+ "values(1,1,0), (2,1,0), (3,1,0), (4,1,0), (5,1,0), (6,1,0),"
				+ "(7,1,1), (8,1,1), (9,1,1), (10,1,1), (11,1,1), (12,1,1),"
				+ "(1,2,0), (2,2,0), (3,2,1), (4,2,0), (5,2,0), (6,2,0),"
				+ "(7,2,0), (8,2,0), (9,2,0), (10,2,0), (11,2,0), (12,2,0),"
				+ "(1,3,0), (2,3,0), (3,3,0), (4,1,0), (5,1,0), (6,1,0),"
				+ "(7,3,0), (8,3,0), (9,3,0), (10,3,0), (11,3,0), (12,3,0)";
	}
	
	public void run() throws SQLException {

			try {
			        connection.prepareStatement(sqlQuery).execute();
			        System.out.println("New data in table prices_extras in DB !");
			
			} catch (Exception e) {
			        // TODO Auto-generated catch block
			        e.printStackTrace();
			} finally {
			        System.out.println("Done !");
			        //connection.close();
			}
			
	}
}
