	package vn.edu.vnuk.airlines.sql;

import java.sql.Connection;
import java.sql.SQLException;

public class SQL_0005_CreateDatabase {
	private final Connection connection;
	private final String sqlQuery;
	
	public SQL_0005_CreateDatabase(Connection connection) {
		this.connection = connection;
		this.sqlQuery = "create database IF NOT EXISTS vnuk_airlines;";
	}
	
	public void run() throws SQLException {

			try {
			        connection.prepareStatement(sqlQuery).execute();
			        System.out.println("New database vnuk_airlines !");
			
			} catch (Exception e) {
			        // TODO Auto-generated catch block
			        e.printStackTrace();
			} finally {
			        System.out.println("Done !");
			}
			
	}
}
