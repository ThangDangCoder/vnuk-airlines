package vn.edu.vnuk.airlines.sql;

import java.sql.Connection;
import java.sql.SQLException;

public class SQL_0060_CreatePlaneModels {
	
	private final Connection connection;
	private final String sqlQuery;
	
	public SQL_0060_CreatePlaneModels(Connection connection) {
		this.connection = connection;
		this.sqlQuery = "create table IF NOT EXISTS plane_models ("
						+ "id BIGINT NOT NULL AUTO_INCREMENT, "
						+ "name VARCHAR(100), "
						+ "plane_manufacture_id BIGINT, "
						+ "primary key (id), "
						+ "FOREIGN KEY (plane_manufacture_id) REFERENCES plane_manufactures(id)"
						+ ");";
	}
	
	public void run() throws SQLException {

			try {
			        connection.prepareStatement(sqlQuery).execute();
			        System.out.println("New table plane_models in DB !");
			
			} catch (Exception e) {
			        // TODO Auto-generated catch block
			        e.printStackTrace();
			} finally {
			        System.out.println("Done !");
			    //    connection.close();
			}
			
	}
}
