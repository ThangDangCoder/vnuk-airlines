package vn.edu.vnuk.airlines.sql;

import java.sql.Connection;
import java.sql.SQLException;

public class SQL_5120_InsertFlights {

	private final Connection connection;
	private final String sqlQuery;
	
	public SQL_5120_InsertFlights(Connection connection) {
		this.connection = connection;
		this.sqlQuery = "insert into flights (route_id,plane_id,day_id,departure_time,arrival_time,flight_code) "
				+ "values(1, 7, 1, '2018-04-26 8:00', '2018-04-26 10:00', 'TAT030'), (4, 4, 4, '2018-05-28 8:00', '2018-05-28 11:50','TAT010'),"
				+ "(2, 3, 6, '2018-03-26 10:00', '2018-03-26 11:15', 'TAT040'), (3, 7, 3, '2018-04-06 9:00', '2018-04-06 10:15', 'TAT050'),"
				+ "(5, 5, 5, '2018-06-16 9:00', '2018-06-16 9:45', 'TAT020'), (6, 7, 4, '2018-05-28 7:00', '2018-05-28 9:00', 'TAT021')";
	}
	
	public void run() throws SQLException {

		try {
		        connection.prepareStatement(sqlQuery).execute();
		        System.out.println("New data in table flights in DB !");
		
		} catch (Exception e) {
		        // TODO Auto-generated catch block
		        e.printStackTrace();
		} finally {
		        System.out.println("Done !");
		        //connection.close();
		}
	}
}

