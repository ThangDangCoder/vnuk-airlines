package vn.edu.vnuk.airlines.sql;

import java.sql.Connection;
import java.sql.SQLException;

public class SQL_0090_CreateAirports {
	
	private final Connection connection;
	private final String sqlQuery;
	
	public SQL_0090_CreateAirports(Connection connection) {
		//super();
		this.connection = connection;
		this.sqlQuery = "create table IF NOT EXISTS airports ("
						+ "id BIGINT NOT NULL AUTO_INCREMENT, "
						+ "code VARCHAR(100), "
						+ "city_id BIGINT, "
						+ "primary key (id), "
						+ "FOREIGN KEY (city_id) REFERENCES cities(id)"
						+ ");";
	}
	
	public void run() throws SQLException {

			try {
			        connection.prepareStatement(sqlQuery).execute();
			        System.out.println("New table airports in DB !");
			
			} catch (Exception e) {
			        // TODO Auto-generated catch block
			        e.printStackTrace();
			} finally {
			        System.out.println("Done !");
			        //connection.close();
			}
			
		}
	
}