package vn.edu.vnuk.airlines.sql;

import java.sql.Connection;
import java.sql.SQLException;

public class SQL_0080_CreateCities {
	
	private final Connection connection;
	private final String sqlQuery;
	
	public SQL_0080_CreateCities(Connection connection) {
		
		this.connection = connection;
		this.sqlQuery = "create table IF NOT EXISTS cities ("
						+ "id BIGINT NOT NULL AUTO_INCREMENT, "
						+ "name VARCHAR(100), "
						+ "country_id BIGINT, "
						+ "primary key (id), "
						+ " FOREIGN KEY (country_id) REFERENCES countries(id)"
						+ ");";
	}
	
	public void run() throws SQLException {

			try {
			        connection.prepareStatement(sqlQuery).execute();
			        System.out.println("New table cities in DB !");
			
			} catch (Exception e) {
			        // TODO Auto-generated catch block
			        e.printStackTrace();
			} finally {
			        System.out.println("Done !");
			        //connection.close();
			}
			
		}
	
}
