package vn.edu.vnuk.airlines.sql;

import java.sql.Connection;
import java.sql.SQLException;



public class SQL_5080_InsertCities {
	private final Connection connection;
	private final String sqlQuery;
	
	public SQL_5080_InsertCities(Connection connection) {
		super();
		this.connection = connection;
		this.sqlQuery = "insert into cities (name,country_id) "
				+ "values('Ha Noi', 1), ('Ho Chi Minh ', 1), ('Da Nang', 1), ('Can Tho', 1), "
				+ "('Seoul', 2)";
	}
	
	public void run() throws SQLException {

			try {
			        connection.prepareStatement(sqlQuery).execute();
			        System.out.println("New data in table cities in DB !");
			
			} catch (Exception e) {
			        // TODO Auto-generated catch block
			        e.printStackTrace();
			} finally {
			        System.out.println("Done !");
			        //connection.close();
			}
			
	}
	
	

}
