package vn.edu.vnuk.airlines.sql;

import java.sql.Connection;
import java.sql.SQLException;


public class SQL_0125_CreateViewPlane {
	
	private final Connection connection;
	private final String sqlQuery;
	
	public SQL_0125_CreateViewPlane (Connection connection) {
		super();
		this.connection = connection;
		this.sqlQuery = "create view plview as "
					  + "select t1.id ,t2.name "
					  + "from planes t1, plane_models t2 where t1.plane_model_id = t2.id;";
	}
	
	public void run() throws SQLException {
		try {
	        connection.prepareStatement(sqlQuery).execute();
	        System.out.println("New table plane_models in DB !");
	
	} catch (Exception e) {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
	} finally {
	        System.out.println("Done !");
	    //    connection.close();
	}
	}
}
