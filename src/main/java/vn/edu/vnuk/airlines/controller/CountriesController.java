package vn.edu.vnuk.airlines.controller;

import java.sql.SQLException;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import vn.edu.vnuk.airlines.dao.CountryDao;
import vn.edu.vnuk.airlines.model.Country;

@Controller
public class CountriesController {

	private final CountryDao dao;
	
	@Autowired
	public CountriesController(CountryDao dao) {
		this.dao = dao;
	}
		
	 @RequestMapping("country/new")
	    public String add(){
	        return "country/new";
	 }
	
	 @RequestMapping("country/create")
	    public String create(@Valid Country country, BindingResult result) throws SQLException{
	        dao.create(country);
	        return "redirect:/countries";
	 }
	 
	 @RequestMapping("countries")
	    public String read(Model model) throws SQLException{
	        model.addAttribute("countries", dao.read());
	        return "country/index";
	 }
	 
	 @RequestMapping("country/show")
	    public String show(@RequestParam Map<String, String> countryId, Model model) throws SQLException{
	        int id = Integer.parseInt(countryId.get("id").toString());
	        model.addAttribute("country", dao.read(id));
	        return "country/show";
	    }
	 
	 @RequestMapping("country/edit")
	    public String edit(@RequestParam Map<String, String> countryId, Model model) throws SQLException{
	        int id = Integer.parseInt(countryId.get("id").toString());
	        model.addAttribute("country", dao.read(id));
	        return "country/edit";
	    }
	 
	 @RequestMapping("country/update")
	    public String update(@Valid Country country, BindingResult result) throws SQLException{
		 	
	        dao.update(country);
	        return "redirect:/countries";
	    }
	 
	//  DELETE WITH AJAX
	    @RequestMapping(value="country/delete", method = RequestMethod.POST)
	    public void delete(int id, HttpServletResponse response) throws SQLException {
	        dao.delete(id);
	        response.setStatus(200);
	    }
}
