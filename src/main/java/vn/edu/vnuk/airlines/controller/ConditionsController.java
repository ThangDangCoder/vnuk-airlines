package vn.edu.vnuk.airlines.controller;

import java.sql.SQLException;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import vn.edu.vnuk.airlines.dao.ConditionDao;
import vn.edu.vnuk.airlines.model.Condition;


@Controller
public class ConditionsController {

	private final ConditionDao dao;
	
	@Autowired
	public ConditionsController(ConditionDao dao) {
		this.dao = dao;
	}
		
	 @RequestMapping("condition/new")
	    public String add(){
	        return "condition/new";
	 }
	
	 @RequestMapping("condition/create")
	    public String create(@Valid Condition condition, BindingResult result) throws SQLException{
	        dao.create(condition);
	        return "redirect:/conditions";
	 }
	 
	 @RequestMapping("conditions")
	    public String read(Model model) throws SQLException{
	        model.addAttribute("conditions", dao.read());
	        return "condition/index";
	 }
	 
	 
	 @RequestMapping("condition/show")
	    public String show(@RequestParam Map<String, String> conditionId, Model model) throws SQLException{
	        int id = Integer.parseInt(conditionId.get("id").toString());
	        model.addAttribute("condition", dao.read(id));
	        return "condition/show";
	 }
	        
	 @RequestMapping("condition/edit")
	    public String edit(@RequestParam Map<String, String> conditionId, Model model) throws SQLException{
	        int id = Integer.parseInt(conditionId.get("id").toString());
	        model.addAttribute("condition", dao.read(id));
	        return "condition/edit";
	    }
	 
	 @RequestMapping("condition/update")
	    public String update(@Valid Condition condition, BindingResult result) throws SQLException{
		 	
	        dao.update(condition);
	        return "redirect:/conditions";
	    }
	//  DELETE WITH AJAX
	    @RequestMapping(value="condition/delete", method = RequestMethod.POST)
	    public void delete(int id, HttpServletResponse response) throws SQLException {
	        dao.delete(id);
	        response.setStatus(200);
	    }

	    
}
