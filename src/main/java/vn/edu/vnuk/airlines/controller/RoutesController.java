package vn.edu.vnuk.airlines.controller;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import vn.edu.vnuk.airlines.dao.AirportDao;
import vn.edu.vnuk.airlines.dao.RouteDao;
import vn.edu.vnuk.airlines.model.Airport;
import vn.edu.vnuk.airlines.model.Route;


@Controller
public class RoutesController {

	private final RouteDao routeDao;
	private final AirportDao airportDao;
	
	@Autowired
	public RoutesController(RouteDao routeDao, AirportDao airportDao) {
		this.routeDao = routeDao;
		this.airportDao = airportDao;
	}
	
	 @RequestMapping("route/new")
	    public String add(Model model){
		 	model.addAttribute("route", new Route());
	        return "route/new";
	 }
	 
	 @RequestMapping("route/create")
	    public String create(@Valid Route route, BindingResult result) throws SQLException{
	        routeDao.create(route);
	        return "redirect:/routes";
	 }
	 
	 @RequestMapping("routes")
	    public String read(Model model) throws SQLException{
	        model.addAttribute("routes", routeDao.read());
	        return "route/index";
	 }
	 
	 @RequestMapping("route/show")
	    public String show(@RequestParam Map<String, String> routeId, Model model) throws SQLException{
	        int id = Integer.parseInt(routeId.get("id").toString());
	        model.addAttribute("route", routeDao.read(id));
	        return "route/show";
	    }
	 
	 @RequestMapping("route/edit")
	    public String edit(@RequestParam Map<String, String> routeId, Model model) throws SQLException{
	        int id = Integer.parseInt(routeId.get("id").toString());
	        model.addAttribute("route", routeDao.read(id));
	        return "route/edit";
	    }
	 
	 @RequestMapping("route/update")
	    public String update(@Valid Route route, BindingResult result) throws SQLException{
	        routeDao.update(route);
	        return "redirect:/routes";
	    }
	 
	//  DELETE WITH AJAX
	    @RequestMapping(value="route/delete", method = RequestMethod.POST)
	    public void delete(int id, HttpServletResponse response) throws SQLException {
	        routeDao.delete(id);
	        response.setStatus(200);
	    }
	    
	    @ModelAttribute("airportList")
	    public Map<Long, String> getAirports(){
	    	Map<Long, String> airportList = new HashMap<Long, String>();
	    	try {
	    		for(Airport airport : airportDao.read()) {
	    			airportList.put(airport.getId(), airport.getCode());
	    		}
	    	}catch (SQLException e) {
	    		e.printStackTrace();
	    	}
	    	return airportList;
	    }
}
