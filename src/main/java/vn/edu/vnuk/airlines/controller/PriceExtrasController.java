package vn.edu.vnuk.airlines.controller;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import vn.edu.vnuk.airlines.dao.ExtraDao;
import vn.edu.vnuk.airlines.dao.PriceDao;
import vn.edu.vnuk.airlines.dao.PriceExtraDao;
import vn.edu.vnuk.airlines.model.Extra;

import vn.edu.vnuk.airlines.model.Price;
import vn.edu.vnuk.airlines.model.PriceExtra;



@Controller
public class PriceExtrasController {

	private final PriceExtraDao priceExtraDao;
	private final PriceDao priceDao;
	private final ExtraDao extraDao;
	
	@Autowired
	public PriceExtrasController(PriceExtraDao priceExtraDao, PriceDao priceDao, ExtraDao extraDao) {
		this.priceExtraDao = priceExtraDao;
		this.priceDao = priceDao;
		this.extraDao = extraDao;
	}
	
	@RequestMapping("priceExtra/new")
    public String add(Model model){
		model.addAttribute("priceExtra", new PriceExtra());
        return "priceExtra/new";
	}
	
	@RequestMapping("priceExtra/create")
    public String create(@Valid PriceExtra priceExtra, BindingResult result) throws SQLException{
        priceExtraDao.create(priceExtra);
        return "redirect:/priceExtras";
	}
	
	@RequestMapping("priceExtras")
    public String read(Model model) throws SQLException{
        model.addAttribute("priceExtras", priceExtraDao.read());
        return "priceExtra/index";
	}
	
	
	@RequestMapping("priceExtra/show")
    public String show(@RequestParam Map<String, String> priceExtraId, Model model) throws SQLException{
        int id = Integer.parseInt(priceExtraId.get("id").toString());
        model.addAttribute("priceExtra", priceExtraDao.read(id));
        return "priceExtra/show";
    }
	
	@RequestMapping("priceExtra/edit")
    public String edit(@RequestParam Map<String, String> priceExtraId, Model model) throws SQLException{
        int id = Integer.parseInt(priceExtraId.get("id").toString());
        model.addAttribute("priceExtra", priceExtraDao.read(id));
        return "priceExtra/edit";
    }
	
	@RequestMapping("priceExtra/update")
    public String update(@Valid PriceExtra priceExtra, BindingResult result) throws SQLException{
        priceExtraDao.update(priceExtra);
        return "redirect:/priceExtras";
    }
	
//  DELETE WITH AJAX
    @RequestMapping(value="priceExtra/delete", method = RequestMethod.POST)
    public void delete(int id, HttpServletResponse response) throws SQLException {
        priceExtraDao.delete(id);
        response.setStatus(200);
    }
    
    @ModelAttribute("priceList")
    public Map<Long, Double> getPrices(){
    	Map<Long, Double> priceList = new HashMap<Long, Double>();
    	try {
    		for(Price price : priceDao.read()) {
    			priceList.put(price.getId(), price.getPrice());
    		}
    	}catch (SQLException e) {
    		e.printStackTrace();
    	}
    	return priceList;
    }
    
    @ModelAttribute("extraList")
    public Map<Long, String> getExtras(){
    	Map<Long, String> extraList = new HashMap<Long, String>();
    	try {
    		for(Extra extra : extraDao.read()) {
    			extraList.put(extra.getId(), extra.getName());
    		}
    	}catch (SQLException e) {
    		e.printStackTrace();
    	}
    	return extraList;
    }
}
