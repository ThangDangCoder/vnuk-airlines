package vn.edu.vnuk.airlines.controller;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import vn.edu.vnuk.airlines.dao.PlaneManufactureDao;
import vn.edu.vnuk.airlines.dao.PlaneModelDao;
import vn.edu.vnuk.airlines.model.PlaneManufacture;
import vn.edu.vnuk.airlines.model.PlaneModel;

@Controller
public class PlaneModelsController {

	private final PlaneModelDao planeModelDao;
	private final PlaneManufactureDao planeManufactureDao;
	
	@Autowired
	public PlaneModelsController(PlaneModelDao planeModelDao,PlaneManufactureDao planeManufactureDao) {
		this.planeModelDao = planeModelDao;
		this.planeManufactureDao = planeManufactureDao;
	}
		
	 @RequestMapping("planeModel/new")
	    public String add(Model model){
		 	model.addAttribute("planeModel", new PlaneModel());
	        return "planeModel/new";
	 }
	
	 @RequestMapping("planeModel/create")
	    public String create(@Valid PlaneModel planeModel, BindingResult result) throws SQLException{
	        planeModelDao.create(planeModel);
	        return "redirect:/planeModels";
	 }
	 
	 @RequestMapping("planeModels")
	    public String read(Model model) throws SQLException{
	        model.addAttribute("planeModels", planeModelDao.read());
	        return "planeModel/index";
	 }
	 @RequestMapping("planeModel/show")
	    public String show(@RequestParam Map<String, String> planeModelId, Model model) throws SQLException{
	        int id = Integer.parseInt(planeModelId.get("id").toString());
	        model.addAttribute("planeModel", planeModelDao.read(id));
	        return "planeModel/show";
	    }
	 
	 @RequestMapping("planeModel/edit")
	    public String edit(@RequestParam Map<String, String> planeModelId, Model model) throws SQLException{
	        int id = Integer.parseInt(planeModelId.get("id").toString());
	        model.addAttribute("planeModel", planeModelDao.read(id));
	        return "planeModel/edit";
	    }
	 
	 @RequestMapping("planeModel/update")
	    public String update(@Valid PlaneModel planeModel, BindingResult result) throws SQLException{
	        planeModelDao.update(planeModel);
	        return "redirect:/planeModels";
	    }
	//  DELETE WITH AJAX
	    @RequestMapping(value="planeModel/delete", method = RequestMethod.POST)
	    public void delete(int id, HttpServletResponse response) throws SQLException {
	        planeModelDao.delete(id);
	        response.setStatus(200);
	    }
	    
	    @ModelAttribute("planeManufactureList")
	    public Map<Long, String> getPlaneManufactures(){
	    	Map<Long, String> planeManufactureList = new HashMap<Long, String>();
	    	 try {
	    		 for(PlaneManufacture planeManufacture : planeManufactureDao.read()) {
	    			 planeManufactureList.put(planeManufacture.getId(), planeManufacture.getName());
	    		 }
	    	 }catch (SQLException e) {
		    	 // TODO Auto-generated catch block
		    	 e.printStackTrace();
	    	 }
	    	 return planeManufactureList;
	    }
	    
}
