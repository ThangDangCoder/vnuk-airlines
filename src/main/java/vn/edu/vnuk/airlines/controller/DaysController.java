package vn.edu.vnuk.airlines.controller;

import java.sql.SQLException;

import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import vn.edu.vnuk.airlines.dao.DayDao;
import vn.edu.vnuk.airlines.model.Day;

@Controller
public class DaysController {
			
		private final DayDao dao;
			
		@Autowired
		public DaysController(DayDao dao) {
			this.dao = dao;
		}
			
		 @RequestMapping("day/new")
		    public String add(){
		        return "day/new";
		 }
		
		 @RequestMapping("day/create")
		    public String create(@Valid Day day, BindingResult result) throws SQLException{
		        dao.create(day);
		        return "redirect:/days";
		 }
		 
		 @RequestMapping("days")
		    public String read(Model model) throws SQLException{
		        model.addAttribute("days", dao.read());
		        return "day/index";
		 }
		 
		 @RequestMapping("day/show")
		    public String show(@RequestParam Map<String, String> dayId, Model model) throws SQLException{
		        int id = Integer.parseInt(dayId.get("id").toString());
		        model.addAttribute("day", dao.read(id));
		        return "day/show";
		 }
		 
		 
		 @RequestMapping("day/edit")
		    public String edit(@RequestParam Map<String, String> dayId, Model model) throws SQLException{
		        int id = Integer.parseInt(dayId.get("id").toString());
		        model.addAttribute("day", dao.read(id));
		        return "day/edit";
		  }
		 
		 @RequestMapping("day/update")
		    public String update(@Valid Day day, BindingResult result) throws SQLException{
			 	
			 	
		        dao.update(day);
		        return "redirect:/days";
		    }
		//  DELETE WITH AJAX
		    @RequestMapping(value="day/delete", method = RequestMethod.POST)
		    public void delete(int id, HttpServletResponse response) throws SQLException {
		        dao.delete(id);
		        response.setStatus(200);
		    }

		    
	}


