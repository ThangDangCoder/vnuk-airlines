package vn.edu.vnuk.airlines.controller;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import vn.edu.vnuk.airlines.dao.ClassDao;
import vn.edu.vnuk.airlines.dao.ConditionDao;
import vn.edu.vnuk.airlines.dao.FlightDao;
import vn.edu.vnuk.airlines.dao.PriceDao;
import vn.edu.vnuk.airlines.dao.SeasonDao;

import vn.edu.vnuk.airlines.model.Flight;
import vn.edu.vnuk.airlines.model.Price;
import vn.edu.vnuk.airlines.model.Season;
import vn.edu.vnuk.airlines.model.Condition;
import vn.edu.vnuk.airlines.model.Classe;

@Controller
public class PricesController {

	private final PriceDao priceDao;
	private final FlightDao flightDao;
	private final ClassDao classDao;
	private final SeasonDao seasonDao;
	private final ConditionDao conditionDao;
	
	
	@Autowired
	public PricesController(PriceDao priceDao, FlightDao flightDao, ClassDao classDao, SeasonDao seasonDao, ConditionDao conditionDao) {
		this.priceDao = priceDao;
		this.flightDao = flightDao;
		this.classDao = classDao;
		this.seasonDao = seasonDao;
		this.conditionDao = conditionDao;
		
	}
	
	 @RequestMapping("price/new")
	    public String add(Model model){
		 	model.addAttribute("price", new Price());
	        return "price/new";
	 }
	 
	 @RequestMapping("price/create")
	    public String create(@Valid Price price, BindingResult result) throws SQLException{
	        priceDao.create(price);
	        return "redirect:/prices";
	 }
	 
	 @RequestMapping("prices")
	    public String read(Model model) throws SQLException{
	        model.addAttribute("prices", priceDao.read());
	        return "price/index";
	 }
	 
	 @RequestMapping("price/show")
	    public String show(@RequestParam Map<String, String> priceId, Model model) throws SQLException{
	        int id = Integer.parseInt(priceId.get("id").toString());
	        model.addAttribute("price", priceDao.read(id));
	        return "price/show";
	  }
	 
	 @RequestMapping("price/edit")
	    public String edit(@RequestParam Map<String, String> priceId, Model model) throws SQLException{
	        int id = Integer.parseInt(priceId.get("id").toString());
	        model.addAttribute("price", priceDao.read(id));
	        return "price/edit";
	    }
	 
	 @RequestMapping("price/update")
	    public String update(@Valid Price price, BindingResult result) throws SQLException{

	        priceDao.update(price);
	        return "redirect:/prices";
	    }
	 
	//  DELETE WITH AJAX
	    @RequestMapping(value="price/delete", method = RequestMethod.POST)
	    public void delete(int id, HttpServletResponse response) throws SQLException {
	        priceDao.delete(id);
	        response.setStatus(200);
	    }
	    
	    @ModelAttribute("flightList")
	    public Map<Long, String> getFlights(){
	    	Map<Long, String> flightList = new HashMap<Long, String>();
	    	try {
	    		for(Flight flight : flightDao.read()) {
	    			flightList.put(flight.getId(), flight.getFlightCode());
	    		}
	    	}catch (SQLException e) {
	    		e.printStackTrace();
	    	}
	    	return flightList;
	    }
	    
	    @ModelAttribute("classList")
	    public Map<Long, String> getClasses(){
	    	Map<Long, String> classList = new HashMap<Long, String>();
	    	try {
	    		for(Classe classe : classDao.read()) {
	    			classList.put(classe.getId(), classe.getName());
	    		}
	    	}catch (SQLException e) {
	    		e.printStackTrace();
	    	}
	    	return classList;
	    }
	    
	    @ModelAttribute("seasonList")
	    public Map<Long, String> getSeasons(){
	    	Map<Long, String> seasonList = new HashMap<Long, String>();
	    	try {
	    		for(Season season : seasonDao.read()) {
	    			seasonList.put(season.getId(), season.getName());
	    		}
	    	}catch (SQLException e) {
	    		e.printStackTrace();
	    	}
	    	return seasonList;
	    }
	    
	    @ModelAttribute("conditionList")
	    public Map<Long, String> getConditions(){
	    	Map<Long, String> conditionList = new HashMap<Long, String>();
	    	try {
	    		for(Condition condition : conditionDao.read()) {
	    			conditionList.put(condition.getId(), condition.getDescription());
	    		}
	    	}catch (SQLException e) {
	    		e.printStackTrace();
	    	}
	    	return conditionList;
	    }
}
